/**
 * Created by chenliang on 2017/1/16.
 */
'use strict';

import React from 'react';
import {
	AppRegistry,
	StyleSheet
} from 'react-native';

import ZhendeAndroidApp from './app/root'

AppRegistry.registerComponent('LMMAndroidApp', () => ZhendeAndroidApp);

