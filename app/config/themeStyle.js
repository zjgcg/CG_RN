import { StyleSheet , Dimensions} from 'react-native';
const {width , height} = Dimensions.get('window');
import themeConfig from './theme';


const  themeStyle = StyleSheet.create({

    bigFontStyle : {
        fontSize: themeConfig.bigSize , 
        color:'white'
    },

    titleFontStyle: {
        fontSize: themeConfig.titleSize, 
        color:'white'
    },

    normalFontStyle: {
        fontSize: themeConfig.normalSize,
        color: 'white'
    },

    commonFontStyle : {
        fontSize: themeConfig.commonSize, 
        color:'red'
    },

    loginBtnStyle : {
        width: width - 60,
        height: 40,
        backgroundColor: '#eeeeee',
        flexDirection:'row' , 
        alignItems:'center' , 
        justifyContent:'center',
        borderRadius:3 
    },

    loginTxtStyle: {
        fontSize : themeConfig.titleSize,
        color : themeConfig.blue1Color
    }, 


    submitBtnStyle : {
        width: width - 60,
        height: 40,
        backgroundColor: themeConfig.blue1Color,
        flexDirection:'row' , 
        alignItems:'center' , 
        justifyContent:'center',
        borderRadius:3 
    },

    printBtnStyle: {
        width: width - 60,
        height: 40,
        backgroundColor: themeConfig.blue3Color,
        flexDirection:'row' , 
        alignItems:'center' , 
        justifyContent:'center',
        borderRadius:3 
    },

    whiteBtnTxtStyle:{
        fontSize : themeConfig.titleSize,
        color : 'white'
    }

});

module.exports = themeStyle;