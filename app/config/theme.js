let themeconfig = {
    blue1Color: '#0066cc',
    blue2Color: '#3399ff',
    blue3Color: '#99ccff',
    grey1Color: '#666666',
    grey2Color: '#999999',
    grey3Color: '#cccccc',
    grey4Color: '#eeeeee',
    red1Color: '#ff6666' ,
    bigSize: 30 ,
    titleSize : 18,
    normalSize: 15,
    commonSize: 14
};

module.exports = themeconfig;
