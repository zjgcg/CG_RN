import React, {
  PureComponent
} from 'react';
import {
  View,
  Text,
  BackAndroid
} from 'react-native';

import {
Navigator
} from 'react-native-deprecated-custom-components'

import Main from './page/main/main'

export default class ZhendeIOSApp extends PureComponent {

  constructor(props) {
    super(props);
    this.navigator;
  }

  componentWillMount() {
  }

  render() {
    return (<View style={{ flex: 1 }}>
      <Navigator
        initialRoute={{ name: '首页', component: Main }}
        style={{ height: 64 }}
        configureScene={(route) => {
          var conf = Navigator.SceneConfigs.HorizontalSwipeJump;
          return conf;
        }}
        renderScene={(route, navigator) => {
          this.navigator = navigator;
          let Component = route.component;
          return (
            <Component navigator={navigator} route={route} {...route.passProps} />
          )
        }
        }
      />
    </View>);
  }
}
