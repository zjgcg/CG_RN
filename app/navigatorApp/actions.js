import { NavigationActions } from 'react-navigation'

export const naviPush = (routeName,params={}) => {
    return dispatch => {
        dispatch(NavigationActions.navigate({
              routeName: routeName,
              params: params,
              action: NavigationActions.navigate({ routeName: routeName})
        }))
    }
};

export const naviBack = (routeName) => {
    return dispatch => {
        if (routeName == undefined){
            dispatch(NavigationActions.back())
        }else{
            dispatch(NavigationActions.back({
                key: routeName
            }))
        }
    }
};


