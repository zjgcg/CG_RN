import {StackNavigator} from 'react-navigation';
import Login from '../page/login/login';
import Main from '../page/main/main';
import ChangePwd from '../page/changepwd/chagePwd';
import Cpgl from '../page/cpgl/cpgl';
import Program from '../page/program/program';
import HistoryList from '../page/historylist/historylist'
import HistoryDetail from '../page/historydetail/historydetail'
import HistoryDetail2 from '../page/historydetail/historydetail2'
import DWJS from '../page/dwjs/dwjs'
import DWJS2 from '../page/dwjs2/dwjs2'
import MemberInfo from '../page/memberInfo/memberInfo'
import NoHandle from '../page/nohandle/nohandle'
import CommonCase from '../page/commoncase/commoncase'

const AppNavigator = StackNavigator({
    Login:{
        screen : Login
    },
    Main: {
        screen: Main,
    },
    ChangePwd: {
        screen : ChangePwd
    },
    Cpgl : {
        screen : Cpgl
    },
    Program: {
        screen : Program
    },
    HistoryList:{
        screen: HistoryList
    },
    HistoryDetail:{
        screen: HistoryDetail
    },
    HistoryDetail2:{
        screen: HistoryDetail2
    },
    DWJS:{
        screen: DWJS
    },
    DWJS2:{
        screen: DWJS2
    },
    MemberInfo:{
        screen:MemberInfo
    },
    NoHandle:{
        screen:NoHandle
    },
    CommonCase:{
        screen:CommonCase
    }
},{
    navigationOptions: {
        gesturesEnabled: false,
    },
}
);

export default AppNavigator;