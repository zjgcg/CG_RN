import {SET_USERGUID , SET_USERNAME , SET_USETTEAM , SET_PRINTER, SET_RECORDNUM , SET_DEPCODE} from './constants';

import NetUtil from '../../common/NetUtil';
import config from '../../config/config';

export const setUserGuid = (userguid) => {
    return dispatch => {
        dispatch({
            type: SET_USERGUID,
            payload: {
                UserGuid: userguid
            }
        })
    }
};


export const setUserName = (name) => {
    return dispatch => {
        dispatch({
            type: SET_USERNAME ,
            payload: {
                userName : name
            }
        })
    }
}

export const setUserTeam = (team) => {
    return dispatch => {
        dispatch({
            type: SET_USETTEAM,
            payload: {
                groupName : team
            }
        })
    }
}


export const setDepCode = (code) => {
    return dispatch => {
        dispatch({
            type : SET_DEPCODE,
            payload : {
                DeptCode: code
            }
        })
    }
}

export const setPrinter = (printerName) => {
    return dispatch => {
        dispatch({
            type: SET_PRINTER,
            payload: {
                printerName: printerName
            }
        })
    }
}



export const GetWeiZhangTCNum = (params) =>{
    let url = '/GetWeiZhangTCNum'
    console.log(params);
    return dispatch => {
    NetUtil.postNew(config.apiUrl + url , params).then(res => {
        console.log('GetWZ')
        console.log(res)
        let ysc = res[0].YSC;
        let wsc = res[0].WSC;
        
            dispatch({
                type: SET_RECORDNUM,
                payload: {
                    yscNum: ysc,
                    wscNum: wsc
                }
            })
       
    }).catch(msg => {
        console.error(msg)
    })
}
}