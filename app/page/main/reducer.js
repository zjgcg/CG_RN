import {SET_USERGUID, SET_PRINTER  , SET_USERNAME , SET_USETTEAM , SET_RECORDNUM , SET_DEPCODE} from './constants';


const initState = {
    UserGuid: "",
    printerName: "未连接",
    userName:'周某某',
    groupName:'杨舍中队',
    DeptCode:'',
    yscNum:0,
    wscNum:0
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case SET_USERGUID:
            return Object.assign({}, state, action.payload);
        case SET_PRINTER:
            return Object.assign({},state , action.payload);
        case SET_USERNAME:
            return Object.assign({} , state , action.payload);
        case SET_USETTEAM:
            return Object.assign({} , state , action.payload);
        case SET_RECORDNUM:
            return Object.assign({} , state , action.payload);
            case SET_DEPCODE:
            return Object.assign({} , state , action.payload);
        default:
            return state;
    }
};

export default reducer;
