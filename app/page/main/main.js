'use strict';
import React from 'react';
import { View  , Dimensions  , Image , Text ,Platform} from 'react-native';
import styles from './styles';
import {connect} from 'react-redux';
import Home from '../home/home';
import Message from '../message/message';
import DWJS from '../dwjs/dwjs'
import Contact from '../contact/contact';
import Setting from '../setting/setting';
import {paddingBottom} from '../../utils/ScreenUtil';
import TabNavigator from 'react-native-tab-navigator';
import { checkContectState , contectPrinter } from '../../utils/NativeUtil';
import config from '../../config/config';
const md5 = require('md5');
import {setPrinter , GetWeiZhangTCNum} from './actions';
class Main extends React.Component {

    constructor(props){
        super(props);
        this.child = undefined;
        this.state = {
            selectedTab: 'home',
            title: "首页",
            unread: "",
            isFirst: false
        }
    }

    componentWillMount() {
        checkContectState({} , res => {
            console.log(res)
            let state = res.state;
            if (state == '0'){
                contectPrinter({} , res => {
                    console.log(res)
                    if (res.state == '1'){
                        this.props.setPrinter(res.name)
                    }
                })
            }
        });
        let params={
            UserGuid : this.props.userGuid,
            KeyWords : md5(this.props.userGuid + config.md5Secret)
        }
        this.props.GetWeiZhangTCNum(params);
    }

    onTabChange = (tab, params, index, key) => {
    };

    static navigationOptions = ({ navigation, screenProps }) => ({
        header: null
    });

    render() {

        let mainView = <View
            style={{backgroundColor: '#f6f6f6', flex: 1, flexDirection: 'column', justifyContent: 'space-between', paddingBottom: paddingBottom}}>
            <TabNavigator>
                <TabNavigator.Item
                    selected={this.state.selectedTab === 'home'}
                    title="首页"
                    titleStyle={styles.tabTextTitleDefault}
                    selectedTitleStyle={styles.tabTextTitleFoucs}
                    renderIcon={() => <Image resizeMode="contain" style={styles.tabItemImage}
                                             source={require('../../image/menu_home_grey.png')}/>}
                    renderSelectedIcon={() => <Image resizeMode="contain" style={styles.tabItemImage}
                                                     source={require('../../image/menu_home_blue.png')}/>}
                    onPress={() => this.setState({selectedTab: 'home', title: "首页"})}>
                    <Home />
                </TabNavigator.Item>
                <TabNavigator.Item
                    selected={this.state.selectedTab === 'message'}
                    title="消息"
                    titleStyle={styles.tabTextTitleDefault}
                    selectedTitleStyle={styles.tabTextTitleFoucs}
                    renderIcon={() => <Image resizeMode="contain" style={styles.tabItemImage}
                                             source={require('../../image/menu_info_grey.png')}/>}
                    renderSelectedIcon={() => <Image resizeMode="contain" style={styles.tabItemImage}
                                                     source={require('../../image/menu_info_blue.png')}/>}
                    onPress={() => this.setState({selectedTab: 'message', title: "消息"})}>
                    <Message />
                </TabNavigator.Item>
                <TabNavigator.Item
                    selected={this.state.selectedTab === 'contact'}
                    title="通讯录"
                    titleStyle={styles.tabTextTitleDefault}
                    selectedTitleStyle={styles.tabTextTitleFoucs}
                    renderIcon={() => <Image resizeMode="contain" style={styles.tabItemImage}
                                             source={require('../../image/menu_address_grey.png')}/>}
                    renderSelectedIcon={() => <Image resizeMode="contain" style={styles.tabItemImage}
                                                     source={require('../../image/menu_address_blue.png')}/>}
                    onPress={() => this.setState({selectedTab:'contact' , title:'通讯录'})}>
                    <DWJS {...this.props}/>
                </TabNavigator.Item>
                <TabNavigator.Item
                    selected={this.state.selectedTab === 'setting'}
                    title="设置"
                    titleStyle={styles.tabTextTitleDefault}
                    selectedTitleStyle={styles.tabTextTitleFoucs}
                    renderIcon={() => <Image resizeMode="contain" style={styles.tabItemImage}
                                             source={require('../../image/menu_setting_grey.png')}/>}
                    renderSelectedIcon={() => <Image resizeMode="contain" style={styles.tabItemImage}
                                                     source={require('../../image/menu_setting_blue.png')}/>}
                    renderBadge={this._chatBadge}
                    onPress={() => {
                        this.setState({selectedTab: "setting" , title:'设置'})
                    }}>
                    <Setting />
                </TabNavigator.Item>
                
            </TabNavigator>
        </View>;

     
        return mainView;
    }
}

const mapStateToProps = state => ({
    userGuid: state.main.UserGuid
});

const mapDispatchToProps = dispatch => ({
    setPrinter: (name) => {
        dispatch(setPrinter(name));
    },
    GetWeiZhangTCNum: (params) => {
        dispatch(GetWeiZhangTCNum(params))
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main);