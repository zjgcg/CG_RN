import {
    StyleSheet
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';

import themeconfig from '../../config/theme'

const styles = StyleSheet.create({
    body: {
        flex: 1,
    },
    shopState: {
        height: 50,
        paddingLeft: 10,
        paddingRight: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        marginBottom:10,
    },
    shopStateText :{
        color:'#5fcc88',
    },
    shopStateButton :{
        color:'#f42733',
        textDecorationLine:'underline',
    },
    head: {
        height: 60,
        marginTop: 30,
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    head_left: {
        left: 30,
        flexDirection: 'row',
        alignItems: 'center',
    },
    head_left_img: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },

    head_left_img2: {
        width: 60,
        height: 50,
        borderRadius: 5,
    },
    head_left_txt2: {
        fontSize: 20,
        marginLeft: 10,
        color: 'white',
    },
    head_left_txt: {
        fontSize: 20,
        marginLeft: 20,
        color: 'white',
    },
    head_right: {
        width: 48,
        right: 50,
        height: 48,
    },
    foot: {
        height: 50,
        marginBottom: 10,
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignItems: 'center',
    },
    foot_left: {
        width: 38,
        height: 50
    },
    foot_middle: {
        width: 38,
        height: 50
    },
    foot_right: {
        width: 38,
        height: 50
    },
    module: {
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        margin: 10,
        width: 125,
        height: 125,
    },
    list: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        margin: 10,
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },
    thumb: {
        width: 80,
        height: 80,
        borderRadius: 5,
        alignSelf: 'center',
    },
    text: {
        marginTop: 5,
        color: 'white',
        fontWeight: 'bold'
    },
    centering: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 80,
    },

    topContainer:{
        paddingLeft:10,
        paddingRight:10,
        backgroundColor: 'white',
    },

    rowContainer: {
        flexDirection: 'row', 
        alignItems: 'center',
        paddingTop:10,
        paddingBottom:10
    },

    colContainer:{
        flex: 1, 
        alignItems: 'center',
    },

    textValue:{
        fontSize:22,
        fontWeight: 'bold',
        color:'black'
    },

    textTitle:{
        fontSize:13,
        fontWeight: 'bold',
        color:'#888888'
    },
    
    orderContainer:{
        marginTop:10,
        height:50,
        flexDirection: 'row', 
        justifyContent: 'space-between',
        backgroundColor:'white',
        alignItems:'center',
        paddingLeft:30,
        paddingRight:30,
    },

    curtainContainer:{
        marginTop:10,
        height:50,
        flexDirection: 'row', 
        justifyContent: 'space-between',
        backgroundColor:'white',
        alignItems:'center',
        paddingLeft:30,
        paddingRight:30,
    },

    liushuiContainer:{
        marginTop:10,
        backgroundColor:'white'
    },

    moduleContainer:{
        marginTop:10,
        paddingTop:20,
        paddingBottom:20,
        backgroundColor:'white'
    },

    modulerowContainer: {
        flexDirection: 'row', 
        flexWrap:'wrap',
        alignItems: 'center',
        paddingTop:10,
        paddingBottom:10
    },

    moduleItemContainer:{
        marginBottom:10,
        alignItems: 'center',
        width:width(33),
    },

    moduleItemImage:{
        width:width(20),
        height:width(20)
    },

    moduleItemTitle:{
        marginTop:5,
        fontSize:15,
        fontWeight: 'bold',
        color:'black'
    },

    tabRowContainer: {
        flexDirection: 'row', 
        alignItems: 'center',
    },

    tabItemImage:{
        marginTop:6,
        width:24,
        height:24
    },

    tabTextTitleFoucs:{
        fontSize:12,
        color:themeconfig.blue2Color
    },

    tabTextTitleDefault:{
        fontSize:12,
        color:themeconfig.grey2Color
    },

    my_head:{
        padding:10,
        backgroundColor:'white',
        flexDirection: 'column', 
        alignItems: 'center',
    },

    my_head_img:{
        marginTop:10,
        width:80,
        height:80,
        borderRadius: 40,
    },

    my_head_text:{
        fontSize:18,
        color:'black',
        marginTop:5,
        marginBottom:5,
    },

    my_row:{
        marginTop:10,
        flexDirection: 'column', 
        backgroundColor:'white'
    },

    my_item:{
        padding:10,
        flexDirection: 'row', 
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    my_item_text:{
        marginLeft:10,
        fontSize:18,
        color:'black',
    },

    logoutButton :{
        backgroundColor:'#f7af2a',
        flexDirection: 'row', 
        justifyContent: 'center',
        alignItems: 'center',
        margin:10,
        height:40,
        borderRadius: 5,
    },

    logoutText:{
        fontSize:18,
        color:"white"
    }
    
});

export default styles;