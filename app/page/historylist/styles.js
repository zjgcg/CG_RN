import React,{Component} from 'react';
import {StyleSheet} from 'react-native';
import themeconfig from '../../config/theme'

const styles= StyleSheet.create({
    TitlelineWrapper:{
        display: 'flex',
        flexDirection: 'column',
        width:'100%',
        flex:1
    },
    Titleline:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginHorizontal: 22,
        marginTop:13,
        borderBottomWidth: 1,
        borderBottomColor:themeconfig.blue2Color
    },
    Titleline_text1:{
        fontSize: 17,
        borderBottomWidth: 2,
        borderBottomColor:themeconfig.blue2Color,
        paddingBottom:5
    },
    Titleline_text2:{
        color:themeconfig.blue2Color
    },
    renderBox:{
        flexDirection:'row',
        justifyContent:'space-between',
        height:100,
        marginHorizontal:22,
        paddingVertical: 10,
        alignItems:'center',
        borderBottomWidth: 1,
        borderBottomColor: themeconfig.grey3Color
    },
    leftContent:{
        flexDirection:'column',
        width: '80%'
    },
    rightContent:{
        flexDirection:'column',
        alignItems:'flex-end'
    },
    list:{
        marginTop:0,
        height:380
    },
    pro:{
        width:68,
        height:30,
        backgroundColor:themeconfig.blue1Color,
        marginTop:10,
        justifyContent:'center',
        alignItems:'center',
        borderRadius: 3
    },
    pro_text:{
        color:'#fff'
    },
    blank1:{
        width:'100%',
        height:5
    }
})

module.exports=styles;