import React,{Component} from 'react';
import {View,Text,ScrollView,FlatList,TouchableOpacity,Image} from 'react-native';
import CLNavigatorHeader from '../../component/CLNavigatorHeader';
import styles from './styles';
import {naviPush} from '../../navigatorApp/actions';
import {connect} from 'react-redux';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import Search from 'react-native-search-box';
import themeconfig from '../../config/theme';
import {getWeiTingList} from './actions';
const md5 = require('md5');
import config from '../../config/config';
import Lists from './components/lists'
import { toast } from '../../utils/NativeUtil';


class HistoryList extends Component{
    static navigationOptions = ({ navigation, screenProps }) => ({
        header: <CLNavigatorHeader navigation={navigation} title="今日违章历史记录"/>
    });

    constructor(props){
        super(props);
        this.state={
            show1:true,
            show2:false,
            data1:[],
            data2:[],
            isLoading: true,
            refreshing:false,
            num1: 0,
            num2: 0 ,
            searchKey:''
        }
        this._data = []
    }

    componentDidMount(){
        if(this.props.navigation.state.params.type=='1'){
            this.setState({
                show1:false,
                show2:true
            })
        }        
    }
 
    handleNumShow1=(num1)=>{
        this.setState({
            num1
        })
    }
    handleNumShow2=(num2)=>{
        this.setState({
            num2
        })
    }
    handleSelect=(index,value)=>{
    
        switch(index){
            case 0:
            this.setState({
                show1:true,
                show2:false
            });
            this.refs.list1.getData();
            break;
            case 1:
            this.setState({
                show1:false,
                show2:true
            });
            this.refs.list2.getData();
            break;
        }
    }

    onSearch = (searchTxt) => {
        this.setState({
            searchKey: searchTxt
        })
        this.refs.list1.getData();
        this.refs.list2.getData();
    }
 
    render(){
        return(
           <View style={{display:'flex' , flexDirection:'column' , flex:1,  backgroundColor:'#eee'}}>           
                <Search 
                 onSearch={this.onSearch}
                backgroundColor='#eee' 
                placeholder='搜索违章记录' 
                inputBorderRadius={18} 
                searchIconCollapsedMargin={45} 
                placeholderCollapsedMargin={33} 
                inputStyle={{backgroundColor:themeconfig.grey3Color,width:'80%'}} 
                cancelButtonTextStyle={{color:themeconfig.grey2Color}} 
                cancelTitle='取消'/>

                <RadioGroup style={{flexDirection:'row',alignItems:'center'}} onSelect={(index,value)=>this.handleSelect(index,value)} 
                selectedIndex={this.props.navigation.state.params.type=='0'?0:1}>
                   <RadioButton value={'item1'} style={{marginLeft:20}}>
                      <Text>已上传记录</Text>
                   </RadioButton>

                   <RadioButton value={'item2'} style={{marginLeft:30}}>
                      <Text>未上传记录</Text>
                   </RadioButton>
                </RadioGroup>
                <View style={[styles.TitlelineWrapper,{display:this.state.show1?'flex':'none'}]}>
                  <Lists  ref="list1"  {...this.props} searchTxt={this.state.searchKey} state='1' show={this.state.show1?'show1':'show2'}  />
                </View>
            
                <View style={[styles.TitlelineWrapper,{display:this.state.show2?'flex':'none'}]}>
                  <Lists  ref="list2"  {...this.props} searchTxt={this.state.searchKey} state='0' show={this.state.show1?'show1':'show2'} />
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => ({
    userGuid: state.main.UserGuid,
    yscNum: state.main.yscNum,
    wscNum : state.main.wscNum
});

const mapDispatchToProps = dispatch => ({
    naviPush: (routeName, params) => {
        dispatch(naviPush(routeName, params));
    },
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HistoryList);
