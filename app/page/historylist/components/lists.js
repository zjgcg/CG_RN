import React,{Component} from 'react';
import {View,Text,ScrollView,FlatList,TouchableOpacity,Image} from 'react-native';
import styles from '../styles';
import {naviPush} from '../../../navigatorApp/actions';
import themeconfig from '../../../config/theme';
import {getWeiTingList} from '../actions';
import config from '../../../config/config';
const md5 = require('md5');

const page = 0;
let hasMore = false;

export default class Lists extends Component {
    constructor(props){
        super(props);
        this.state={
            data:[],
            isLoading: true,
            refreshing:false,
        }
        this._data = []
    }
    componentDidMount(){
        page = 0;
        this.getData();
    }

    getData = () => {
        let params = {
            UserGuid: this.props.userGuid,
            SearchInfo: this.props.searchTxt || '',
            KeyWords:md5(this.props.userGuid + config.md5Secret),
            PageIndex: page,
            state: this.props.state
        }
        console.log(params)
        getWeiTingList(params).then(res => {
            console.log(res)
            if (res.length < 10) {
                hasMore = false
            } else {
                hasMore = true
            }
            if (page == 0) {
                this._data = res;
            } else {
                this._data = this._data.concat(res);
            }
            // if(res.length>0){
                this.setState({
                    data:this._data,
                    isLoading: false,
                    refreshing: false,
                })
            // }
            // else{
            //     this.setState({
            //         data:[],
            //         isLoading: false
            //     })
            // }
        }).catch(err => {
            console.error(err)
        });
    }

 

    onEndReached = () => {
        if (hasMore) {
            page++;
            this.getData()
        }
    }//上拉加载更多
    
    onRefresh = () => { //刷新回调函数
        this.setState({
            refreshing: true,
            isLoading: true
        })
        page = 0;
        this.getData()
    }

    _keyExtractor = (item, index) => index;

    handlePress=(index)=>{
        if(this.props.show=='show1'){
            this.props.navigation.navigate('HistoryDetail',{rowguid:this.state.data[index].RowGuid , reload:this.getData})
        }else{
            this.props.navigation.navigate('HistoryDetail2',{rowguid:this.state.data[index].RowGuid ,  reload:this.getData})
        }
    }
    _renderItem=({item,index})=>{
        return(
            <TouchableOpacity onPress={(i)=>{i=index;this.handlePress(i)}}>
            <View style={styles.renderBox}>
                <View style={styles.leftContent}>
                    <Text style={{color:themeconfig.blue2Color}}>{'贴单编号：'+item.TDBH}</Text>
                    <View style={styles.blank1} />
                    <Text numberOfLines={1} ellipsizeMode='tail'>{'车牌号：'+item.CP}</Text>
                    <View style={styles.blank1} />
                    <Text numberOfLines={1} ellipsizeMode='tail'>{'地点：'+item.WZDD}</Text>
                    <View style={styles.blank1} />
                    <Text style={{color:themeconfig.grey2Color,fontSize:14}}>{item.JCSJ}</Text>
                </View>
                <View style={styles.rightContent}>
                <Image source={require('../../../image/icon_right.png')} style={{width:24, height:24}} />
                </View>
            </View>
            </TouchableOpacity>
        )
    }
    render(){
        return (
            <View style={styles.TitlelineWrapper}>
            <View style={styles.Titleline}>
                <Text style={styles.Titleline_text1}>{this.props.show=='show1'?'已上传记录':'未上传记录'}</Text>
                <Text style={styles.Titleline_text2}>共{this.props.show=='show1'?this.props.yscNum:this.props.wscNum}起</Text>
            </View>
            <FlatList 
            data={this.state.data} 
            renderItem={this._renderItem} 
            onEndReached={this.onEndReached} 
            onEndReachedThreshold={0.2} 
            onRefresh={this.onRefresh} 
            refreshing={this.state.isLoading}
            style={styles.list} 
            keyExtractor={this._keyExtractor}/>
            </View>
        )
    }
} 
