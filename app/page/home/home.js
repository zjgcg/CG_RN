'use strict';
import React from 'react';
import { View,  ImageBackground , Dimensions , Image , Text , Platform , TouchableOpacity , ScrollView,RefreshControl} from 'react-native';
import {naviPush} from '../../navigatorApp/actions';
import {connect} from 'react-redux';
import NetUtil from '../../common/NetUtil';
import themeConfig from '../../config/theme'
import { getConfig  , setBlueTooth} from '../../utils/NativeUtil';
import Styles  from  './styles';
import config from '../../config/config';
const {width , height} = Dimensions.get('window');
import moment from 'moment';
import {GetDepartUserInfo} from './actions'
import {setPrinter,GetWeiZhangTCNum} from '../main/actions';
const md5 = require('md5');

class Home extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            pariState : '未配对',
            userguid:'',
            departmentCode:'',
            flag:false,
            num1:'',
            num2:'',
            refreshing:false
        }
    }


    componentDidMount(){
        getConfig('pariState',res=>{
            console.log(res);
            if(res.pariState==''){
                this.setState({
                    pariState:'未配对'
                })
            }else{
            this.setState({
                pariState:res.pariState
            })
        }
        })
        getConfig('userguid' , res => {
            console.log(res);
            let params={
                UserGuid:res.userguid
            }
            GetDepartUserInfo(params).then(res2=>{
                console.log('info',res2)
                console.log(res2[0].DeptCode)
                this.setState({
                    departmentCode:res2[0].DeptCode
                })
            }).catch(msg=>{
                console.log(msg)
            })
            this.setState({
                userguid:res.userguid
            })
        })
    }

    _onRefresh=()=>{
        this.setState({refreshing : true})
        let params={
            UserGuid : this.props.userGuid,
            KeyWords : md5(this.props.userGuid + config.md5Secret)
        }
        var run = function(){
            let url = '/GetWeiZhangTCNum'
            var _promise = new Promise(function(resolve,reject){
                setTimeout(function(){
                NetUtil.postNew(config.apiUrl + url , params).then(res => {
                    console.log('res',res)
                    if(res){
                        resolve('resolve'+params)
                    }else{
                        reject('reject'+params)
                    }
                })
            },300)
            })
            return _promise;
        }
        let self = this;
        run().then(function(data){
            self.props.GetWeiZhangTCNum(params);
            self.setState({refreshing:false})
        console.log(data)
        }).catch(msg=>{
            console.log(msg)
        })
    }

    static navigationOptions = ({ navigation, screenProps }) => ({
        header: null
    });

    toViewByRouter = (router,params={}) => {
        this.props.naviPush(router,params)
    }

    setBlueToothAction = () => {
        setBlueTooth(res => {
            console.log(res);
            if (res.name && res.name.length > 0 ){
                this.props.setPrinter(res.name)
            }
        });
    }

    render () {

        let today = moment().format('YYYY-MM-DD');
        let paddingTopSize = 0;
        if (Platform.OS == 'ios'){
            paddingTopSize = 24;
        }else{
            paddingTopSize = 0;
        }

        return <ScrollView refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh}/>}>
            <ImageBackground style={{flexDirection:'column' ,flex:1 , alignItems:'center' , paddingTop: paddingTopSize}} source={require('../../image/bg_home.png')}>
            <View style={{flexDirection:'column' , width: width - 40 , marginTop:20}}>
                <TouchableOpacity 
                    onPress={this.setBlueToothAction}
                style={{flexDirection:'row' , alignItems:'center' , marginBottom:8}}>
                    <Image style={Styles.iconStyle} source={require('../../image/ico_print.png')}></Image>
                    <Text style={Styles.iconTxtStyle}>{this.props.printerName}</Text>
                </TouchableOpacity>
                <View style={{flexDirection:'row' , alignItems:'center'}}>
                <Image style={Styles.iconStyle} source={require('../../image/ico_pair.png')}></Image>
                <Text style={Styles.iconTxtStyle} onPress={()=>this.toViewByRouter('DWJS2',{departmentCode:this.state.departmentCode, teaminfo:'' , abovelevel:''})}>{this.state.pariState}</Text>
                </View>
            </View>
            <View style={{marginTop:-30 , flexDirection:'column' , alignItems:'center'}}>
                <Image 
                source={require('../../image/ico_logo.png')}
                style={{width:60 , height:67  , marginBottom:10}}
            />
            <Text style={[Styles.groupStyle,{marginBottom:5}]}>{this.props.groupName}</Text>
            <Text style={Styles.nameStyle}>{this.props.userName}</Text>
            </View>

            <View style={{width: width - 40 , height: 200 , backgroundColor:'white' , marginTop:30 , flexDirection:'row' , shadowColor:themeConfig.blue1Color , shadowOffset:{width:1,height:2},shadowOpacity:0.8 ,  elevation: 4}}>
                <View style={{flex:1 , flexDirection:'column'}}>
                    <TouchableOpacity 
                    onPress={()=>this.toViewByRouter('Cpgl')}
                    style={{flex:1 , flexDirection:'row' , alignItems:'center' , paddingLeft:20 , paddingRight:20 }}>
                        <Image source={require('../../image/ico_cpgl.png')} style={[Styles.moduleImgStyle,{marginRight:10}]}></Image>
                        <View style={{flexDirection:'column'}}>
                           <Image source={require('../../image/title_cpgl.png')} style={[Styles.moduleTitleStyle,{marginBottom:5}]}></Image>
                           <Text style={Styles.moduleInfoStyle}>违章违停车辆抄牌管理</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{height:1 , width: (width - 40) /2 , backgroundColor: themeConfig.blue3Color}}></View>
                    <TouchableOpacity onPress={()=>this.toViewByRouter('DWJS',{param:'home'})} style={{flex:1 ,  flexDirection:'row' , paddingLeft:20 , paddingRight:20 , alignItems:'center'}}>
                    <Image source={require('../../image/ico_dwjs.png')} style={[Styles.moduleImgStyle,{marginRight:10}]}></Image>
                        <View style={{flexDirection:'column'}}>
                           <Image source={require('../../image/title_dwjs.png')} style={[Styles.moduleTitleStyle,{marginBottom:5}]}></Image>
                           <Text style={Styles.moduleInfoStyle}>执法队伍人员组织结构</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{width:1 , backgroundColor:themeConfig.blue3Color , height:200}}>
                </View>
                <View style={{flex:1 , flexDirection:'column'}}>
                <TouchableOpacity 
                onPress={()=>this.toViewByRouter('Program')}
                style={{flex:1 ,  flexDirection:'row' , paddingLeft:20 , paddingRight:20 , alignItems:'center'}}>
                    <Image source={require('../../image/ico_jycx.png')} style={[Styles.moduleImgStyle,{marginRight:10}]}></Image>
                        <View style={{flexDirection:'column'}}>
                           <Image source={require('../../image/title_jycx.png')} style={[Styles.moduleTitleStyle,{marginBottom:5}]}></Image>
                           <Text style={Styles.moduleInfoStyle}>违章现场处罚管理</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{height:1 , width: (width - 40) /2 , backgroundColor: themeConfig.blue3Color}}></View>
                    <TouchableOpacity 
                    onPress={()=>this.toViewByRouter('HistoryList',{type:'0'})}
                    style={{flex:1 ,   flexDirection:'row' , paddingLeft:20 , paddingRight:20 , alignItems:'center'}}>
                    <Image source={require('../../image/ico_lsjl.png')} style={[Styles.moduleImgStyle,{marginRight:10}]}></Image>
                        <View style={{flexDirection:'column'}}>
                           <Image source={require('../../image/title_lsjl.png')} style={[Styles.moduleTitleStyle,{marginBottom:5}]}></Image>
                           <Text style={Styles.moduleInfoStyle}>今日违章抄牌历史记录</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
           

            <View style={{height:30 , flexDirection:"row" ,  width: width - 40 , marginTop:10,  justifyContent:'space-between' , alignItems:'center'}}>
                    {/* <Text style={Styles.normalStyle} onPress={()=>{this.setState({flag:!this.state.flag})}}>{this.state.flag?'今日抄牌':'123'}</Text> */}
                    <Text style={Styles.normalStyle}>今日抄牌</Text>
                    <Text style={Styles.dateTitleStyle}>{today}</Text>
            </View>

            <TouchableOpacity onPress={()=>this.toViewByRouter('HistoryList',{type:'0'})}>
            <ImageBackground source={require('../../image/bg_ysc.png')} resizeMode="contain"  style={{width: width - 40 , height:(width - 40) / 702 * 159  , borderRadius:3 , flexDirection:"row" , alignItems:"center",paddingBottom:8}}>
                <Text style={[Styles.recordTxtStyle , {marginLeft:15 , marginRight:10 , backgroundColor:'transparent'}]}>已上传记录</Text>
                <ImageBackground source={require('../../image/bg_round1.png')} style={Styles.roundStyle}>
                    <Text style={[Styles.roundTitleStyle,{backgroundColor:'transparent'}]}>{this.props.yscNum}</Text>
                </ImageBackground>
                <View style={{flex:1 , height:1 }}></View>
                <View style={{height:80 , marginRight:17}}>
                    <Text style={[Styles.yscTipStyle,{backgroundColor:'transparent'}]}>查看记录</Text>
                </View>
            </ImageBackground>
            </TouchableOpacity>

            <TouchableOpacity onPress={()=>this.toViewByRouter('HistoryList',{type:'1'})}>
            <ImageBackground source={require('../../image/bg_wsc.png')} resizeMode="contain" 
            style={{width: width - 40 , height:(width - 40) / 702 * 159 , borderRadius:3 , flexDirection:"row" , alignItems:"center",paddingBottom:8 , marginBottom:20}}>
            <Text style={[Styles.recordTxtStyle , {marginLeft:15 , marginRight:10,backgroundColor:'transparent'}]}>未上传记录</Text>
                <ImageBackground source={require('../../image/bg_round2.png')} style={[Styles.roundStyle,{backgroundColor:'transparent'}]}>
                    <Text style={[Styles.roundTitleStyle,{backgroundColor:'transparent'}]}>{this.props.wscNum}</Text>
                </ImageBackground>
                <View style={{flex:1 , height:1}}></View>
                <View style={{height:80 , marginRight:17}}>
                    <Text style={[Styles.yscTipStyle,{backgroundColor:'transparent'}]}>马上上传</Text>
                </View>
            </ImageBackground>
            </TouchableOpacity>

        </ImageBackground>
        </ScrollView>
    }
}

const mapStateToProps = state => ({
    printerName: state.main.printerName,
    groupName: state.main.groupName,
    userName : state.main.userName,
    userGuid: state.main.UserGuid,
    yscNum: state.main.yscNum,
    wscNum : state.main.wscNum
});

const mapDispatchToProps = dispatch => ({
    naviPush: (routeName, params) => {
        dispatch(naviPush(routeName, params));
    },
    setPrinter: (name) => {
        dispatch(setPrinter(name));
    },
    GetWeiZhangTCNum: (params) => {
        dispatch(GetWeiZhangTCNum(params))
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);