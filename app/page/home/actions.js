import NetUtil from '../../common/NetUtil';
import config from '../../config/config';

export const GetDepartUserInfo = (params) => {
    let url = '/GetDepartUserInfo'
    return NetUtil.postNew(config.apiUrl + url , params);
}


