import { StyleSheet } from 'react-native';
import themeConfig from '../../config/theme';

const styles = StyleSheet.create({
    iconStyle : {
        width : 24 ,
        height : 24 , 
        marginRight:3
    },
    iconTxtStyle:{
        fontSize : 12,
        color: 'white',
        backgroundColor:'transparent'
    },
    groupStyle: {
        color:'white',
        fontSize: themeConfig.commonSize , 
        backgroundColor:'transparent'
    },
    nameStyle:{
        color:'white',
        fontSize: themeConfig.normalSize,
        backgroundColor:'transparent'
    },
    moduleImgStyle: {
        width: 46 , 
        height: 46
    },
    moduleTitleStyle: { 
        height: 15,
        width: 60,
    },
    moduleInfoStyle: {
        fontSize: 10,
        width: 60,
    },

    normalStyle : {
        color:'#333333',
        fontSize: themeConfig.normalSize,
        backgroundColor:'transparent'
    },

    dateTitleStyle: {
        fontSize: themeConfig.commonSize,
        color: themeConfig.blue3Color,
        backgroundColor:'transparent'
    },
    recordTxtStyle: {
        fontSize : themeConfig.titleSize,
        color: themeConfig.blue3Color,
        backgroundColor:'transparent'
    },
    roundStyle: {
        flexDirection:"row" ,
        alignItems:'center',
        justifyContent:'center',
        width:60,
        height:60
    },
    roundTitleStyle:{
        color:'white',
        fontSize: themeConfig.titleSize + 2,
        backgroundColor:'transparent',
        fontWeight:'bold'
    },

    yscTipStyle:{
        fontSize: themeConfig.commonSize,
        color: themeConfig.blue2Color,
        backgroundColor:'transparent',
        marginTop: 8
    },

    shadeView : {
        width: 120,
        backgroundColor:'#000',
        height:120,
        shadowColor:'#f00',
        shadowOffset:{height:10,width:10},
        shadowRadius:3,
        shadowOpacity:0.8,
        elevation: 4
    }
});

module.exports = styles;