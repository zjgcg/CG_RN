import React,{Component} from 'react';
import {View,Text,ScrollView,TouchableOpacity,Image,TextInput} from 'react-native';
import CLNavigatorHeader from '../../component/CLNavigatorHeader';
import {connect} from 'react-redux';
import { getConfig ,choosePhoto, toast} from '../../utils/NativeUtil';
import Styles from '../cpgl/styles';
import styles from '../program/styles';
import themeconfig from '../../config/theme';
import ChoosePhotoView from '../compents/ChoosePhotoView';
import ActionSheet from 'react-native-actionsheet';
import BigButton from '../../component/BigButton'
import {Message_Update} from '../commoncase/actions'
import { ArrayUtil } from '../../utils/common';
import config from '../../config/config';
import {getRandOrderNo} from '../cpgl/actions'
const md5 = require('md5');
import {naviPush , naviBack} from '../../navigatorApp/actions';

const CANCEL_INDEX = 0;
const imageOptions = ['取消', '拍照上传', '选择图片'];
class NoHandle extends Component {
    static navigationOptions = ({ navigation, screenProps }) => ({
        header: <CLNavigatorHeader navigation={navigation} title="无需处理"/>
    });

    constructor(props){
        super(props);
        console.log(props);
        this.state={
            orderNo:'自动生成',
            t2:'',
            detail:'',
            tjzp: [], //添加照片
        }
        this.RowGuid=''
    }

    chooseItemClick = (item, tag) => {
        this.photoLstTag = tag;
        this.phtotoItem = item;
        this.ImageActionSheet.show();
    };

    getRandOrderNoAction  = () => {
        if (this.state.orderNo == '自动生成'){
            getRandOrderNo(this.props.userGuid).then(res => {
                console.log(res)
                let data = res[0];
                this.RowGuid = data.RowGuid;
            }).catch(msg => {
                console.error(msg)
            });
        }
    }

    handleImagePress = (i) => {
        let type = "choose";
        let size_w = 0;
        let size_h = 0;
        if (i == 1) {
            type = "takephoto"
        } else if (i == 2) {
            type = "choose"
        } else {
            return;
        }
        
        let params = {
            type : type,
            fileType: 'wxcl',
            recordGuid: this.RowGuid
        }
        choosePhoto(params , data => {
             let imgs = data.imgs;
             if (imgs != ''){
                let lst = this.state.tjzp;
                lst.push({url: imgs , id: lst.length  , title: ''});
                this.setState({
                    tjzp: lst
                });
             }
        });
    }

    delPhotos = (item , index) => {
        console.log(item)
        console.log(index)
        let photos = this.state.tjzp;
        ArrayUtil.delete(photos , item);
        this.setState({tjzp:photos})
    }

    submit=()=>{
        let params={
            RowGuid : this.props.navigation.state.params.rowguid,
            UserGuid : this.props.userGuid,
            Password : md5(this.props.userGuid + config.md5Secret),
            Remark : this.state.detail,
            type : '04'
        }
        console.log('params',params)
            Message_Update(params).then(res=>{
                if (this.props.navigation.state.params.reload != undefined){
                    this.props.navigation.state.params.reload();
                }
                toast('提交成功')
                this.props.navigation.state.params.reload();
                // this.props.naviBack();
            }).catch(msg=>{
                console.log(msg)
            })
        // this.props.naviPush('Message',{})
    }

    componentDidMount(){
        this.getRandOrderNoAction();
        getConfig('pariState',res=>{
            console.log(res);
            this.setState({
                orderNo: this.props.navigation.state.params.CaseNo,
                t2: this.props.userName+','+res.pariState
            })
        });
    }
    render(){
        return (
            <View style={styles.totalbox}>
                <View style={Styles.itemView}>
                    <Text style={Styles.lableText}>派单号</Text>
                    <TouchableOpacity style={Styles.centerView} onPress={this.getRandOrderNoAction}>
                        <Text style={[Styles.rightValue]}>{this.state.orderNo}</Text>
                    </TouchableOpacity>
                </View>
                <View style={Styles.itemView}>
                <Text style={Styles.lableText}>配对队员</Text>
                    <TextInput 
                    value={this.state.t2} 
                    onChangeText={(txt)=>this.setState({t2:txt})}
                    style={[styles.t1,{color:themeconfig.blue2Color}]} placeholder='郭德纲，于谦' placeholderTextColor={themeconfig.blue2Color}/>
                </View>
                <View style={Styles.itemView}>
                <Text style={Styles.lableText}>详细描述</Text>
                    <TextInput 
                    value={this.state.t3} 
                    onChangeText={(txt)=>this.setState({detail:txt})}
                    style={[styles.t1,{color:themeconfig.blue2Color}]} multiline={true} numberOfLines={5} underlineColorAndroid='transparent'/>
                </View>
                <View style={[Styles.itemView,{flexDirection:'column',alignItems:'flex-start',marginTop: 10}]}>
                    <Text style={Styles.lableText}>添加照片</Text>
                    <View style={{width:'100%',height:60,marginTop: 15,flexDirection:'row'}}>
                    <ChoosePhotoView 
                    onItemClick={(item) => this.chooseItemClick(item, 'wxcl')}
                    delAction={(item , index)=>this.delPhotos(item, index)}
                    images={this.state.tjzp}
                    num={3}
                    maxNum={3}
                    itemheight={80}
                />
                <ActionSheet
                ref={o => this.ImageActionSheet = o}
                title="选择图片"
                options={imageOptions}
                cancelButtonIndex={CANCEL_INDEX}
                onPress={this.handleImagePress}/>
                    </View>
                </View>
                <BigButton label='提交' backgroundColor={themeconfig.blue1Color} style={{marginTop: 35}} onPress={this.submit}/>
            </View>
        )
    }
}

const mapStateToProps = state => ({
    userName: state.main.userName,
    userGuid: state.main.UserGuid
});

const mapDispatchToProps = dispatch => ({
    naviPush: (routeName, params) => {
        dispatch(naviPush(routeName, params));
    },
    naviBack: (routeName) => {
        dispatch(naviBack(routeName))
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NoHandle);