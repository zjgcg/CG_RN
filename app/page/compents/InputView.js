import React from 'react';
import { View , Text , TouchableOpacity , Image } from 'react-native';
import themeconfig from '../../config/theme'

export default class NormalLabelView  extends React.Component {
    constructor(props){
        super(props);
        this.leftValue = props.leftValue;
        this.rightValue = props.rightValue;
        this.rightHint = props.rightHint;
        this.rightBtnValue = props.rightBtnValue;
        this.rightBtnAction = props.rightBtnAction;
        this.itemClickAction = props.itemClickAction;
    }

    render(){

        let rightBtnView = <TouchableOpacity 
        onPress={this.rightBtnAction}
        style={[Styles.rightBtn, this.props.rightBtnStyle]}>
            <Text style={Styles.rightBtnTxt}>{this.rightBtnValue || ''}</Text>
        </TouchableOpacity>

        let rightTxtView =  <View style={{flexDirection:'row' , alignItems:'center'}}>
        {
            this.rightValue ? <Text style={[Styles.rightValue]}>{this.rightValue}</Text> : 
            <Text style={[Styles.rightHint]}>{this.rightHint}</Text>
        }
        <Image source={require('../../image/icon_right.png')} style={[Styles.rightIcon]}></Image>
    </View>

        return  <TouchableOpacity style={[Styles.itemView , this.props.itemViewStyle ]} onPress={this.itemClickAction}>
        <Text style={Styles.lable}>{this.leftValue}</Text>
        {
            this.rightBtnValue ? rightBtnView : rightTxtView
        }
    </TouchableOpacity>
    }
}

const Styles = {
    itemView :{
        flexDirection:'row' ,
        justifyContent:'space-between' , 
        alignItems:"center",
        marginHorizontal:15,
        minHeight:48,
        borderBottomWidth:1 , 
        borderBottomColor:'#ccc'
    },
    lable: {
        color: '#666666',
        fontSize: themeconfig.normalSize
    },
    rightIcon: {
        width : 16,
        height: 16
    },
    rightValue : {
        color: themeconfig.blue2Color,
        fontSize: themeconfig.normalSize,
        marginRight:10,
    },
    rightHint: {
        color: themeconfig.blue2Color,
        fontSize: themeconfig.normalSize,
        marginRight:10
    },
    rightBtn: {
        flexDirection:'row' ,
        justifyContent:'center' , 
        alignItems:"center",
        paddingHorizontal:8 , 
        paddingVertical:5 , 
        backgroundColor:themeconfig.blue1Color , 
        borderRadius:3
    },
    rightBtnTxt: {
        color:'white' ,
        fontSize:themeconfig.normalSize , 
    }

}
