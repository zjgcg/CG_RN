/**
 * 选择弹出框
 */
import React from 'react';
import {View  , Modal , Text ,  TouchableOpacity , Dimensions , FlatList} from 'react-native';
const {width , height}  = Dimensions.get('window');
import themeConfig from '../../config/theme';

export default class ChooseModal extends React.Component{

    constructor(props){
        super(props);
    }

    _renderItem = ({item , index}) => {
        return(<TouchableOpacity onPress={() => this.props.onSure(item , index)} 
        style={[{marginHorizontal:10 , height: 40 , justifyContent:'center' , flex:1 },this.props.itemStyle]}>
            <Text>{item}</Text>
        </TouchableOpacity>)
    }

    _ItemSeparatorComponent = () => {
        return <View style={{height:1 , width:this.modalWidth , backgroundColor:'#ebebeb'}}></View>
    }

    render(){
        let  modalVisible = this.props.modalVisible;
        let modalWidth = this.props.width || width - 30;
        let flatLstHeight = this.props.height || 200;
        let numColumns = this.props.numColumns || 1;

        return (<Modal 
        visible={modalVisible}
        animationType={"none"}
        transparent={true}
        onRequestClose={() => this.props.hideAction()}>
        <TouchableOpacity  
            onPress={() => this.props.hideAction()} style={{flexDirection: 'column', 
        justifyContent:'center', alignItems:'center',
        flex: 1 , backgroundColor: 'rgba(10,10,10,0.4)'}}>
            <View style={{width: modalWidth , flexDirection:'column' , backgroundColor:'white' , borderRadius:3}}>
                {
                    this.props.title ? <View style={{height:48 , width: modalWidth , flexDirection:'row' , alignItems:'center' , justifyContent:'center'}}>
                        <Text style={{color:themeConfig.grey1Color , fontSize:themeConfig.titleSize}}>{this.props.title}</Text>
                    </View> : null
                }

                <FlatList 
                    data={this.props.lstStr}
                    numColumns={numColumns}
                    style={{height: flatLstHeight , width: modalWidth }}
                    renderItem={this._renderItem}
                    ItemSeparatorComponent={this._ItemSeparatorComponent}
                />
            </View>
        </TouchableOpacity>
        </Modal>)
    }
}