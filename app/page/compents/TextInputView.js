import React from 'react';
import {View , Text , TouchableOpacity} from 'react-native';
import themeConfig from '../../config/theme';


export default class TextInputView extends React.Component{
    constructor(props){
        super(props);
    }



    render(){
        let btnViews = null;
        if (this.props.btnValue){
            btnViews = <TouchableOpacity 
            onPress={this.props.rightBtnAction}
            style={[Styles.rightBtn,this.props.rightBtnStyle]}>
            <Text style={[Styles.rightBtnTxt, this.props.rightBtnTxtStyle]}>{this.props.btnValue}</Text>
            </TouchableOpacity>
        }
        return(<View style={Styles.itemView}>
            <Text style={Styles.lableText}>{this.props.keyStr}</Text>
            <TouchableOpacity style={{flex:1 , borderBottomColor:'#ccc' , borderBottomWidth:1}}>
                {
                      this.rightValue ? <Text style={[Styles.rightValue]}>{this.rightValue}</Text> : 
                      <Text style={[Styles.rightHint]}>{this.rightHint}</Text>
                }
            </TouchableOpacity>
            {btnViews}
        </View>)
    }
}

const Styles = {
    itemView: {
        flexDirection:'row' , 
        alignItems:'center' , 
        minHeight:48
    },
    centerView: {
        
    },


    lableText:{
        fontSize:themeConfig.commonSize , 
        color:themeConfig.grey1Color ,
        width: 120,
    },
    rightValue : {
        color: themeConfig.blue2Color,
        fontSize: themeConfig.normalSize,
        marginRight:10,
    },
    rightHint: {
        color: themeConfig.blue2Color,
        fontSize: themeConfig.normalSize,
        marginRight:10
    },
    rightBtn: {
        flexDirection:'row' ,
        justifyContent:'center' , 
        alignItems:"center",
        paddingHorizontal:8 , 
        paddingVertical:5 , 
        backgroundColor:themeConfig.blue1Color , 
        borderRadius:3
    },
    rightBtnTxt: {
        color:'white' ,
        fontSize:themeConfig.normalSize , 
    }
}