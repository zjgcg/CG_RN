/**
 * 图片选择
 */
import React from 'react';
import {View, TouchableOpacity, Dimensions, Image, Text} from 'react-native';
import LmmImageView from '../../component/LmmImageView';
const {width, height} = Dimensions.get('window');

export  default class ChoosePhotoView extends React.Component {

    constructor(props) {
        super(props);
        this.Number = props.num || 3;
        this.ItemHeight = props.itemheight || 60;
        this.maxNum = props.maxNum || 99;
    }

    render = () => {
        let self = this;
        let images = this.props.images;
        let imgviews = images.map((item, i) => {
            return self.renderItem(item, i);
        });
        
        if (images.length < this.maxNum) {
            imgviews.push(this.renderItem({url:''}))
        }

        return (<View
            style={{flexWrap: 'wrap', flexDirection: 'row', justifyContent: 'flex-start',}}>
            {imgviews}
        </View>)
    };


    itemClick = (item) => {
        this.props.onItemClick(item);       
    };


    delItemAction = (item , index ) => {
        this.props.delAction(item , index )
    }

    renderItem = (item, i) => {
        let itemWidth = (width - 40 - (this.Number - 1) * 10 ) / this.Number;
        let itemview;
        let txtview;
        if (item.title && item.title.length > 0) {
            txtview = (<Text style={{fontSize: 12}}>{item.title}</Text>);
        }
        if (item.url == '') {
            itemview = (
                <View style={{flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
                    <Image source={require('../../image/add_51.png')} style={{width: 30, height: 30, marginBottom: 3}}/>
                    {txtview}
                </View>);
        } else {
            itemview = (<View style={{width: itemWidth,
                height: this.ItemHeight}}>
                <LmmImageView source={{uri: item.url}} style={{
                width: itemWidth,
                height: this.ItemHeight,
            }}/>
            {/* <TouchableOpacity  
                style={{position:"absolute" , top:0, right:0, width:30 , height:30 , justifyContent:'center' , alignItems:'center'}} onPress={()=>this.delItemAction(item , i)}>
                <Image source={require('../../image/icon_del_green.png')} style={{width:20 , height:20}}></Image>
            </TouchableOpacity> */}
            </View>);
        }

        if ((i + 1) % this.Number > 0) {
            return (
                <TouchableOpacity
                    key={i}
                    onPress={() => this.itemClick(item)}
                    style={{
                        width: itemWidth,
                        height: this.ItemHeight,
                        backgroundColor: '#eee',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                        borderColor: '#eee',
                        marginRight: 10,
                        marginBottom: 10
                    }}>
                    {itemview}
                </TouchableOpacity>);
        } else {
            return (
                <TouchableOpacity
                    key={i}
                    onPress={() => this.itemClick(item)}
                    style={{
                        width: itemWidth,
                        height: this.ItemHeight,
                        backgroundColor: '#eee',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                        borderColor: '#eee',
                        marginBottom: 10
                    }}>
                    {itemview}
                </TouchableOpacity>);
        }
    };
}

