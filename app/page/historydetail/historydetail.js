import React,{Component} from 'react';
import {View,Text,Image,TouchableOpacity,ScrollView} from 'react-native';
import CLNavigatorHeader from '../../component/CLNavigatorHeader';
import {connect} from 'react-redux';
import {naviPush} from '../../navigatorApp/actions';
import hdstyles from './styles'
import Styles from '../cpgl/styles'
import BigButton from '../../component/BigButton'
import themeconfig from '../../config/theme'
import {GetWeiZhangTCInfo} from './actions'
import config from '../../config/config';
const md5 = require('md5');

class HistoryDetail extends Component {
    static navigationOptions = ({ navigation, screenProps }) => ({
        header: <CLNavigatorHeader navigation={navigation} title="已上传记录详情"/>
    });
    constructor(props){
        super(props);
        this.state={
            datainfo:{}
        }
    }


    componentDidMount(){
        let params={
            UserGuid : this.props.userGuid,
            keyWords: md5(this.props.userGuid + config.md5Secret),
            RowGuid : this.props.navigation.state.params.rowguid
        }
        GetWeiZhangTCInfo(params).then(res=>{
            console.log(res)
            this.setState({
                datainfo: res[0]
            })
        }).catch(msg=>{
            console.log(msg)
        })
    }
    render(){
        return(
            <ScrollView style={hdstyles.totalbox}>
                <View style={hdstyles.titbox}>
                <Image source={require('../../image/ico_time.png')} style={hdstyles.titImg} />
                   <Text style={hdstyles.tit}>{this.state.datainfo.JCSJ} 上传</Text>
                </View>
                <View style={hdstyles.list}>
                    <View style={[Styles.itemView,{minHeight:35}]}>
                        <Text style={hdstyles.leftfont}>贴单编码</Text>
                        <Text style={hdstyles.rightfont}>{this.state.datainfo.TDBH}</Text>
                    </View>
                </View>
                <View style={hdstyles.list}>
                    <View style={[Styles.itemView,{minHeight:35}]}>
                        <Text style={hdstyles.leftfont}>号牌种类</Text>
                        <Text style={hdstyles.rightfont}>{this.state.datainfo.HPZL}</Text>
                    </View>
                </View>
                <View style={hdstyles.list}>
                    <View style={[Styles.itemView,{minHeight:35}]}>
                        <Text style={hdstyles.leftfont}>车牌号&emsp;</Text>
                        <Text style={hdstyles.rightfont}>{this.state.datainfo.CP}</Text>
                    </View>
                </View>
                <View style={hdstyles.list}>
                    <View style={[Styles.itemView,{minHeight:35}]}>
                        <Text style={hdstyles.leftfont}>违停方向</Text>
                        <Text style={hdstyles.rightfont}>{this.state.datainfo.WTFX}</Text>
                    </View>
                </View>
                <View style={hdstyles.list}>
                    <View style={[Styles.itemView,{minHeight:35}]}>
                        <Text style={hdstyles.leftfont}>违章地点</Text>
                        <Text style={hdstyles.rightfont} numberOfLines={2}>{this.state.datainfo.WZDD}</Text>
                    </View>
                </View>
                <View style={hdstyles.list}>
                    <View style={[Styles.itemView,{minHeight:35}]}>
                        <Text style={hdstyles.leftfont}>配对队员</Text>
                        <Text style={hdstyles.rightfont}>{this.state.datainfo.PDRY}</Text>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = state => ({
    userGuid: state.main.UserGuid
});

const mapDispatchToProps = dispatch => ({
    naviPush: (routeName, params) => {
        dispatch(naviPush(routeName, params));
    },
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HistoryDetail);