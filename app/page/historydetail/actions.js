import NetUtil from '../../common/NetUtil';
import config from '../../config/config';

export const GetWeiZhangTCInfo= (params) => {
    let url = '/GetWeiZhangTCInfo'
    return NetUtil.postNew(config.apiUrl + url , params);
}

export const WeiZhangTC_Update = (params) => {
    let url = '/WeiZhangTC_Update'
    return NetUtil.postNew(config.apiUrl + url , params);
}

export const Cpgl_Printer_New = (params) => {
    const method  = "/WeiZhangTC_PrintNew";
    return NetUtil.postNew(config.apiUrl + method , params);
}


/**
 * 获取 违停参数
 */
export const getWtContent = (guid) => {
    const  method = "/GetWTContentByUserGuid"
    return NetUtil.postNew(config.apiUrl + method , {UserGuid: guid});
}