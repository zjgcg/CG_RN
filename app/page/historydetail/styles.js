import React,{Component} from 'react';
import {StyleSheet} from 'react-native';
import themeconfig from '../../config/theme';

const hdstyles = StyleSheet.create({
    totalbox:{
        flex:1
    },
    titbox:{
        height:46,
        paddingVertical:8,
        flexDirection: 'row',
        justifyContent:'flex-end',
        marginRight: 22,
        alignItems:'center'
    },
    titImg:{
        width:20,
        height:20,
        marginRight:5
    },
    tit:{
        fontSize: 14,
        color:themeconfig.grey2Color
    },
    list:{
        paddingHorizontal: 15,
        flexDirection:'column'
    },
    leftfont:{
        fontSize: 16
    },
    rightfont:{
        color: themeconfig.blue2Color,
        marginLeft: 30,
        fontSize: 16,
        width: '68%'
    },
    place:{
        flexDirection:'column'
    },
    blank1:{
        width:'100%',
        height: 5
    }
})

module.exports=hdstyles;