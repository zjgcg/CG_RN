import React from 'react';
import { View , TextInput , TouchableOpacity } from 'react-native';
import CLNavigatorHeader from '../../component/CLNavigatorHeader';
import {connect} from 'react-redux';
import {naviPush} from '../../navigatorApp/actions';
import themeconfig from '../../config/theme';
import themStyle  from  '../../config/themeStyle';
class ChangePwd extends React.Component{

    static navigationOptions = ({ navigation, screenProps }) => ({
        header: <CLNavigatorHeader navigation={navigation} title="密码修改"/>
    });

    constructor(props){
        super(props);
        this.state = {
            oldPwd:'',
            newPwd:'',
            newAgainPwd:''
        }
    }


 
    render(){
        return <View>
            <View style={Styles.itemViewStyle}>
                <Text style={Styles.keyTxtStyle}>旧密码</Text>
                <View style={Styles.valueViewStyle}>
                    <TextInput 
                        underlineColorAndroi={'transparent'}
                        placeholder="原登录密码"
                        placeholderTextColor={{color:'#eee' , fontSize:themeconfig.commonSize}}
                        style={{fontSize:themeconfig.commonSize}}
                    ></TextInput>
                </View>
            </View>
            <View style={Styles.itemViewStyle}>
                <Text style={Styles.keyTxtStyle}>新密码</Text>
                <View style={Styles.valueViewStyle}>
                    <TextInput 
                        underlineColorAndroi={'transparent'}
                        placeholder="输入新密码"
                        placeholderTextColor={{color:'#eee' , fontSize:themeconfig.commonSize}}
                        style={{fontSize:themeconfig.commonSize}}
                    ></TextInput>
                </View>
            </View>

            <View style={Styles.itemViewStyle}>
                <Text style={Styles.keyTxtStyle}>再次输入</Text>
                <View style={Styles.valueViewStyle}>
                    <TextInput 
                        underlineColorAndroi={'transparent'}
                        placeholder="再次输入密码"
                        placeholderTextColor={{color:'#eee' , fontSize:themeconfig.commonSize}}
                        style={{fontSize:themeconfig.commonSize}}
                    ></TextInput>
                </View>
            </View>

        <TouchableOpacity 
            style={[themStyle.loginBtnStyle,{marginTop:25 }]}>
                <Text style={themStyle.loginTxtStyle}>确定</Text>
            </TouchableOpacity>

        </View>
    }
}

const  Styles = {
    itemViewStyle: {
        flexDirection:'row', 
        alignItems:'center' , 
        height: 48 ,
        marginHorizontal:18
    },
    keyTxtStyle : {
        marginRight:10
    },
    valueViewStyle: {borderBottomWidth:1 , borderBottomColor:'#CCCCCC' , paddingHorizontal:5}
};


const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
    naviPush: (routeName, params) => {
        dispatch(naviPush(routeName, params));
    },
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChangePwd);