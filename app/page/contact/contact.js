'use strict';
import React from 'react';
import {View , Text} from 'react-native';
import {naviPush} from '../../navigatorApp/actions';
import {connect} from 'react-redux';
import CLNavigatorHeader from "../../component/CLNavigatorHeader";

/**
 * 通讯录Tab
 */
class Contact extends React.Component {

    static navigationOptions = ({ navigation, screenProps }) => ({
        header: <CLNavigatorHeader navigation={navigation} title="通讯录"/>
    });

    render(){
        return <View><Text>Contact</Text></View>
    }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
    naviPush: (routeName, params) => {
        dispatch(naviPush(routeName, params));
    },
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Contact);