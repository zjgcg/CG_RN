import {StyleSheet} from 'react-native';
import themeconfig from '../../config/theme';

const mistyles= StyleSheet.create({
    header:{
        flex:1,
        flexDirection:'row',
        backgroundColor:themeconfig.blue1Color,
        paddingHorizontal:22,
        paddingTop:35,
        paddingBottom:13
    },
    header_Content1:{
        flexDirection:'column',
        width:'40%'
    },
    header_Content2:{
        flexDirection:'column',
        width:'60%'
    },
    font_down:{
        fontSize:27,
        color:"#fff"
    },
    main:{
        flex:6,
        flexDirection:'column',
        // marginHorizontal:12,
        marginLeft:5,
        marginTop:22
    },
    leftContent:{
        borderBottomWidth:1,
        borderBottomColor:themeconfig.grey3Color,
        minHeight:50,
        flexDirection:'column',
        justifyContent:'center'
    }
})

module.exports=mistyles;