import React,{Component} from 'react';
import {View,Text,ScrollView,Image,TouchableOpacity,Linking} from 'react-native';
import mistyles from './styles';
import CLNavigatorHeader from '../../component/CLNavigatorHeader';
import {naviPush} from '../../navigatorApp/actions';
import {connect} from 'react-redux';
import themeconfig from '../../config/theme';
import dwstyles from '../dwjs/styles'
import back_white from '../../image/back_white.png'
import { GetDepartUserInfo } from '../home/actions';

class MemberInfo extends Component {
    static navigationOptions = ({ navigation, screenProps }) => ({
        header: <CLNavigatorHeader navigation={navigation} title="队员信息" style={{backgroundColor:themeconfig.blue1Color,borderBottomColor:themeconfig.blue1Color}} 
        titleStyle={{color:'#fff'}} leftImage={require('../../image/left.png')} leftItem={null}/>
    });
    constructor(props){
        super(props);
        this.state={
            tel:'',
            fixphone:'',
            memberdetail:{}
        }
    }
    
    componentDidMount(){
        let params= {
            UserGuid : this.props.navigation.state.params.userguid
        }
        GetDepartUserInfo(params).then(res=>{
            console.log('userinfo',res);
            this.setState({
                memberdetail: res[0]
            })
        }).catch(msg=>{
            console.log(msg)
        })
    }
    render(){
        return (
            <ScrollView style={{flex:1}}>
                <View style={mistyles.header}>
                    <View style={mistyles.header_Content1}><Text style={{color:'#fff'}}>姓名</Text><Text style={mistyles.font_down}>{this.state.memberdetail.DisplayName}</Text></View>
                    <View style={mistyles.header_Content2}><Text style={{color:'#fff'}}>警号</Text><Text style={mistyles.font_down}>ZJG-3467218</Text></View>
                </View>
                <View style={mistyles.main}>
                <View style={dwstyles.listitem}>
                    <View style={mistyles.leftContent}><Text>性别&emsp;&emsp;</Text></View>
                    <View style={[dwstyles.rightContent,{marginLeft:0,paddingLeft:60}]}><Text>{this.state.memberdetail.Sex=='1'?'男':'女'}</Text></View>
                </View>
                <View style={dwstyles.listitem}>
                    <View style={mistyles.leftContent}><Text>出生日期</Text></View>
                    <View style={[dwstyles.rightContent,{marginLeft:0,paddingLeft:60}]}><Text>{this.state.memberdetail.Birthday}</Text></View>
                </View>
                <View style={dwstyles.listitem}>
                    <View style={mistyles.leftContent}><Text>所属部门</Text></View>
                    <View style={[dwstyles.rightContent,{marginLeft:0,paddingLeft:60}]}><Text>{this.state.memberdetail.Department}</Text></View>
                </View>
                <View style={dwstyles.listitem}>
                    <View style={mistyles.leftContent}><Text>职务&emsp;&emsp;</Text></View>
                    <View style={[dwstyles.rightContent,{marginLeft:0,paddingLeft:60}]}><Text>{this.state.memberdetail.Duty}</Text></View>
                </View>
                <View style={dwstyles.listitem}>
                    <View style={mistyles.leftContent}><Text>固定电话</Text></View>
                    <View style={[dwstyles.rightContent,{marginLeft:0,paddingLeft:60}]}><Text>{this.state.memberdetail.Tel}</Text>
                    <TouchableOpacity onPress={()=>{return Linking.openURL('tel:'+this.state.memberdetail.Tel)}}><Image source= {require('../../image/ico_tel.png')} style={{width:30,height:30}}/></TouchableOpacity>
                    </View>
                </View>
                <View style={dwstyles.listitem}>
                    <View style={mistyles.leftContent}><Text>手机&emsp;&emsp;</Text></View>
                    <View style={[dwstyles.rightContent,{marginLeft:0,paddingLeft:60}]}>
                       <Text>{this.state.memberdetail.Mobile}</Text>
                       <View style={{flexDirection:'row'}}>
                          <TouchableOpacity onPress={()=>{Linking.openURL('smsto:'+this.state.memberdetail.Mobile)}}><Image source= {require('../../image/ico_message.png')} style={{width:30,height:30,marginRight:10}}/></TouchableOpacity>
                          <TouchableOpacity onPress={()=>{return Linking.openURL('tel:'+this.state.memberdetail.Mobile)}}><Image source= {require('../../image/ico_tel.png')} style={{width:30,height:30}}/></TouchableOpacity>
                       </View>
                    </View>
                </View>
                </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
    naviPush: (routeName, params) => {
        dispatch(naviPush(routeName, params));
    },
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MemberInfo);