import NetUtil from '../../common/NetUtil';
import config from '../../config/config';

export const GetAllDepartment = (params ) => {
    let url = '/GetAllDepartment'
    return NetUtil.postNew(config.apiUrl + url , params);
}