import {StyleSheet} from 'react-native';
import themeconfig from '../../config/theme';

const dwstyles = StyleSheet.create({
    totalbox: {flex:1,backgroundColor:'#eee'},
    listitem:{
        marginHorizontal: 22,
        flexDirection:'row',
        minHeight: 50,
        alignItems:'center'
    },
    rightContent:{
        minHeight: 50,
        borderBottomWidth: 1 ,
        borderBottomColor: themeconfig.grey3Color,
        width:'86%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        marginLeft:13
    },
    rightContent_text:{
    },
    view2box:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'rgba(0, 0, 0, 0.3)',
    },
    view2:{
        flexDirection:'column',
        marginHorizontal: 22
    },
    listitem2:{
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomWidth:1,
        borderBottomColor:themeconfig.grey3Color,
        minHeight:40,
        alignItems:'center'
    },
    footerimg:{
        width:'100%',
        height: 100,
        alignItems:'center',
        justifyContent:'center'
    }
})

module.exports= dwstyles;