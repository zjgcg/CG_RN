import {StyleSheet} from 'react-native';
import themeconfig from '../../config/theme';

const styles = StyleSheet.create({
    totalbox:{
        flex:1,
        paddingHorizontal:15 ,
        backgroundColor:'#fff',
        flexDirection:'column',
    },
    t1:{
        width: '68%',
        flex:1 , 
        color: themeconfig.blue2Color,
        borderBottomColor:'#ccc' , 
        borderBottomWidth:1,
        paddingVertical: 0,
    },
    cfdd:{
        flexDirection:'row' , 
        minHeight:48,
        marginVertical: 18
    },
    _t4:{
        flexDirection:'column',
        justifyContent:'center',
        flex:1,
        borderBottomColor:'#ccc' , 
        borderBottomWidth:1
    },
    t4_city:{
        color:themeconfig.blue2Color
    },
    t4:{
       flex:1,
       paddingVertical: 0,
       color: themeconfig.blue2Color,
       borderBottomColor:'#ccc' , 
       borderBottomWidth:1
    },
    picker:{
        width: 150 ,
        height: 30,
        backgroundColor:'#ebebeb',
        borderWidth: 1,
        borderRadius: 5
    },
    _t6:{
        flexDirection:'row',
        alignItems:'center',
        width:'100%',
        borderBottomColor:'#ccc' , 
        // borderBottomWidth:1,
        // paddingVertical: 0,
    },
    t6:{
        flex:1,
        borderBottomColor:'#ccc' , 
        borderBottomWidth:1,
        paddingVertical: 0,
        color: themeconfig.blue2Color
    },
    t6_text:{
        color:themeconfig.blue2Color
    },
    t7:{
        width: '54%',
    },
    bt:{
        width: 50 ,
        height: 28,
        backgroundColor: themeconfig.blue1Color,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:4
    },
    bt_label:{
        fontSize: 14,
        fontWeight: 'bold',
        color:'#fff'
    },
    t9:{
        color:themeconfig.blue2Color,
        marginTop: 5,
        width:'54%'
    },
    //两个大按钮样式
    bt_view:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },
    submit:{
        width: '80%',
        height: 40 ,
        margin: 8,
        backgroundColor: themeconfig.blue1Color,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5
    },
    print:{
        width: '80%',
        height: 40,
        backgroundColor: themeconfig.blue3Color,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5
    },
    //空白，高度由小到大
    blank1:{
        width:'100%',
        height: 5
    },
    blank2:{
        width:'100%',
        height: 10
    },
    blank3:{
        width:'100%',
        height: 20
    },
  
})

export default styles;