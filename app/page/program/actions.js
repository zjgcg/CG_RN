import NetUtil from '../../common/NetUtil';
import config from '../../config/config';


/**
 * 简易程序
 */
export const getRandOrderNo = (userGuid) => {
    const method = '/JYCX_Num';
    return NetUtil.postNew(config.apiUrl + method , {UserGuid: userGuid});
}


export const getProgrameAnYou = (params) => {
    const method = '/JYCX_AnYou'
    return NetUtil.postNew(config.apiUrl + method , params);
}

export const addProgram = (params) => {
    const method = '/JWCX_Add'
    return NetUtil.postNew(config.apiUrl + method , params);
}