import NetUtil from '../../common/NetUtil';
import config from '../../config/config';


/**
 * 自动生成贴单编号
 */
export const getRandOrderNo = (userGuid) => {
    const method = '/GetTDNum';
    return NetUtil.postNew(config.apiUrl + method , {UserGuid: userGuid});
}


export const Cpgl_Add = (params) => {
    const  method = "/WeiZhangTC_Add";
    return NetUtil.postNew(config.apiUrl + method , params);
}

export const Cpgl_Printer = (params) => {
    const method  = "/WeiZhangTC_Print";
    return NetUtil.postNew(config.apiUrl + method , params);
}

/**
 * 获取 违停参数
 */
export const getWtContent = (guid) => {
    const  method = "/GetWTContentByUserGuid"
    return NetUtil.postNew(config.apiUrl + method , {UserGuid: guid});
}