import React from 'react';
import { View  , Text , Image, ScrollView , TouchableOpacity , TextInput , Alert , Dimensions , Platform} from 'react-native';
import {naviPush , naviBack} from '../../navigatorApp/actions';
import {connect} from 'react-redux';
import CLNavigatorHeader from '../../component/CLNavigatorHeader';
import TextInputView from '../compents/TextInputView';
import ChooseModal from '../compents/ChooseModal';
import themeConfig from '../../config/theme';
import Styles from './styles';
import themStyle  from  '../../config/themeStyle';
const md5 = require('md5');
import moment  from 'moment';
const carType = ['小型汽车','大型汽车']
const directions = ['其他','西-北','西-南','东-北','东-南','东-西','南-北','西-东','北-南'];
import { getRandOrderNo , Cpgl_Add ,Cpgl_Printer , getWtContent} from './actions';
import config from '../../config/config';
import { showLoading , hideLoading , getConfig , getLocation , setBlueTooth ,printer, toast  ,  choosePhoto, setConfigValue , microPhotoListener} from '../../utils/NativeUtil';
import {setPrinter , GetWeiZhangTCNum} from '../main/actions';
const imageOptions = ['取消', '拍照上传'];
const CANCEL_INDEX = 0;
import ChoosePhotoView from '../compents/ChoosePhotoView';
import ActionSheet from 'react-native-actionsheet';
let {width }  = Dimensions.get('window');
const carCity = ["苏","浙","沪","皖","赣","京","津","渝","黑","吉","辽","蒙","冀","新","甘","青","陕","宁","豫","鲁","晋","鄂","湘","川","贵","云","桂","藏","粤","闽","台","琼"]; //"港","澳"
const carCityNo = ['A' , 'B' , 'C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z'];
import { ArrayUtil } from '../../utils/common';
import {stringToUtf8ByteArray , utf8ByteArrayToString} from '../../utils/StringUtil';

let TelPhone ="";
let CLAddress = "";

class Cpgl extends React.Component {

    static navigationOptions = ({navigation, screenProps}) => {
        const {params = {}} = navigation.state;
        let header = <CLNavigatorHeader navigation={navigation} title="抄牌管理" leftAction={params.backAction}/>;
        return {header};
    };

    constructor(props){
        super(props);
        this.RowGuid = '';
        this.state = {
            orderNo:'自动生成',
            carTypeVisible:false,
            carTypeValue:'小型汽车',
            car_cp: '苏',
            car_city_cp:'E', // 城市
            car_no: '' ,// 车牌号
            car_direction:'',
            detailAddress:'',//详细地址
            carDirectionVisible: false , //停车方向对话框
            carDirectionValue: '其他',
            photos:[],
            showCarCity:false,
            showCityNo:false,
            pariPersonName:'',
            printed:false,
            showRemark: false,
            remark:''
        };
        this.JinDu = 0;
        this.WeiDu = 0;
        this.fromType = props.navigation.state.params.fromType || ""; // message
        this.CaseNo = "";
        this.WtContent = undefined
    }

    componentDidMount(){
        this.props.navigation.setParams({backAction: this._backAction});
        this.getRandOrderNoAction();
        if (this.fromType == "message"){
            this.CaseNo = this.props.navigation.state.params.CaseNo;
            this.setState({showRemark : true})
        }

        // this.setCLAddres();
        getWtContent(this.props.userGuid).then(res => {
            // console.log('getWtContent' , res)
            if (res.length>0) {
                this.WtContent = res[0]
            }
            // console.log(this.WtContent)
        }).catch(msg => {
            console.error(msg)
        })

        getConfig('pariState' , res => {
            console.log(res);
            this.setState({
                pariPersonName :  this.props.userName + ',' + res.pariState
            })
        });

        getLocation(res => {
            if (res == null || res == undefined) {
        
            } else {
                this.JinDu = res.lon;
                this.WeiDu = res.lat;
            }

        
            getConfig('wzdd' , conf => {
                let lastAddress = conf.wzdd;
                if (lastAddress.length > 0){
                    this.setState({
                        detailAddress: lastAddress
                    });
                }else {
                    let address  =  '张家港市' + res.address;
                    this.setState({
                        detailAddress: address
                    });
                }
            });

            getConfig('wtfx',res => {
                let lastFx = res.wtfx;
                this.setState({carDirectionValue:lastFx || '其他'});
            });
        })
    }

    getLocationAction = () => {
        getLocation(res => {
            if (res == null || res == undefined) {
                return;
            }
            this.JinDu = res.lon;
            this.WeiDu = res.lat;
            let address  =  '张家港市' + res.address;

            this.setState({
                detailAddress: address
            });
    });
    }

    setCLAddres(){
        let DeptCode = this.props.depCode;
        if(DeptCode == "032001")
		{
			TelPhone="0512-58305786";
			CLAddress="张家港市城市管理行政执法局处理窗口（白鹿路99号）";
		}
		if(DeptCode ==  "032002")
		{
			TelPhone="0512-58697718、0512-58305791";
			CLAddress="张家港市城市管理行政执法局处理窗口（白鹿路99号）";
		}
		if(DeptCode == "032003")
		{
			TelPhone="0512-55395819";
			CLAddress="张家港市城市管理行政执法局处理窗口（白鹿路99号）";
		}
		if(DeptCode == "032004")
		{
			TelPhone="0512-58305793、0512-58671595";
			CLAddress="张家港市城市管理行政执法局处理窗口（白鹿路99号）";
		}
		if(DeptCode ==  "032005")
		{
			TelPhone="0512-58305783";
			CLAddress="张家港市城市管理行政执法局处理窗口（白鹿路99号）";
		}
		if(DeptCode ==  "032006")
		{
			TelPhone="0512-58690778";
			CLAddress="张家港市城市管理行政执法局处理窗口（白鹿路99号）";
		}
		if(DeptCode ==  "033001")
		{
			TelPhone="0512-58117802、0512-58117222";
			CLAddress="张家港市金港镇中圩西路13号";
		}
		if(DeptCode  ==  "033002")
		{
			TelPhone="0512-56991781、0512-56991782";
			CLAddress="张家港市杨舍镇塘市镇中路工商所东侧弄堂、塘市西菜场西侧";
		}
		if(DeptCode  ==  "033003")
		{
			TelPhone="0512-58106080";
			CLAddress="张家港市塘桥镇202县道（老204国道）800号";
		}
		if(DeptCode ==  "033004")
		{
			TelPhone="0512-58950119";
			CLAddress="张家港市锦丰镇锦花路72号";
		}
		if(DeptCode ==  "033005")
		{
			TelPhone="0512-58422115";
			CLAddress="张家港市凤凰镇张市路66号";
		}
		if(DeptCode ==  "033006")
		{
			TelPhone="0512-58960161";
			CLAddress="张家港市乐余镇人民路张家港市农村商业银行西侧向北50米";
		}
		if(DeptCode ==  "033007")
		{
			TelPhone="0512-58907229、18013610165";
			CLAddress="张家港市南丰镇社会管理服务中心";
		}
		if(DeptCode ==  "033008")
		{
			TelPhone="0512-56936225";
			CLAddress="张家港市大新镇星光路";
		}
		if(DeptCode ==  "033009")
		{
			TelPhone="0512-58645891";
			CLAddress="张家港市现代农业示范园区红旗路";
		}
    }

    getRandOrderNoAction  = () => {
        if (this.state.orderNo == '自动生成'){
            getRandOrderNo(this.props.userGuid).then(res => {
                console.log(res)
                let data = res[0];
                this.setState({orderNo : data.TDBH});
                this.RowGuid = data.RowGuid;
            }).catch(msg => {
                console.error(msg)
            });
        }
    }

    _backAction = () => {
        Alert.alert('提示',
            "是否返回",
        [
            {text: '否', onPress: () => {}},
            {text: '是', onPress: () => { this.props.naviBack();}},
        ]);
    };

    checkValue = () => {
        if (this.state.car_no.length < 5 || this.state.car_no.length > 6){
            toast('请输入完整车牌');
            return false;
        }

        if (this.state.detailAddress.length == 0) {
            toast('请输入地址');
            return false;
        }

        if (this.state.photos.length < 3){
            toast('请至少上传三张照片')
            return false;
        }

        return true;
    }

    isCarNo = (carNoF) => {
        const reg = /^([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领 A-Z]{1}[A-HJ-NP-Z]{1}(([0-9]{5}[DF])|([DF]([A-HJ-NP-Z0-9])[0-9]{4})))|([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领 A-Z]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9 挂学警港澳]{1})$/;
        return reg.test(carNoF);
    };

    printerAction = () => {

         // 判断蓝牙连接状态
         if (this.props.blueToothState == '未连接') {
            toast ('请先连接打印机')
            return;
        }


        // if (this.state.car_no.length < 5 || this.state.car_no.length > 6){
        //     toast('请输入完整车牌');
        //     return ;
        // } else {
            // 车牌正则判断
            let carNoF = this.state.car_cp + this.state.car_city_cp + this.state.car_no
            let issuccess = this.isCarNo(carNoF)
            console.log('carNoF' , carNoF)
            console.log('issuccess',issuccess)
            if (!this.isCarNo(carNoF)){
                toast('请输入正确的车牌');
                return
            }
               
        // }



        if (this.state.detailAddress.length == 0) {
            toast('请输入地址');
            return ;
        }




        let params  = {
            RowGuid: this.RowGuid,
            TDBH: this.state.orderNo,
            HPZL: this.state.carTypeValue,
            CPHM: this.state.car_cp + this.state.car_city_cp + this.state.car_no,
            SZLD:"",	
            RoadGuid:'',	
            WZDD: this.state.detailAddress,	
            WTFX: this.state.carDirectionValue,
            JCSJ: moment().format('YYYY-MM-DD HH:mm:ss'),
            ZFRY: this.props.userName,
            ZFRYGuid: this.props.userGuid,	
            PDRY: this.state.pariPersonName,	
            JinDu: this.JinDu,
            WeiDu: this.WeiDu,
            Password: md5(this.JinDu + config.md5Secret),

        }
        console.log(params);
        Cpgl_Printer(params).then(res => {
            console.log(res)
            this.toPrintAction(); 
            this.updateRecordNum();
        }).catch(msg => {
            toast(msg);
        });
    }


    toPrintAction = () => {

        // 判断蓝牙连接状态
        if (this.props.blueToothState == '未连接') {
            toast ('请先连接打印机')
            return;
        }

        let carNo_b = stringToUtf8ByteArray(this.state.car_cp + this.state.car_city_cp + this.state.car_no);
        let carNo = utf8ByteArrayToString(carNo_b);

        let TDBH_b = stringToUtf8ByteArray(this.state.orderNo);
        let TDBH = utf8ByteArrayToString(TDBH_b);

        let date_b = stringToUtf8ByteArray(moment().format("YYYY年MM月DD日HH时mm分"));
        let date = utf8ByteArrayToString(date_b);

        let detailAddress_b = stringToUtf8ByteArray(this.state.detailAddress);
        let detailAddress = utf8ByteArrayToString(detailAddress_b);

        // let Tel_b = stringToUtf8ByteArray(TelPhone);
        // let Tel = utf8ByteArrayToString(Tel_b);

        let cl_b = stringToUtf8ByteArray(CLAddress);
        let cl = utf8ByteArrayToString(cl_b);

        let printParams = {
            carNo: carNo ,
            TDBH: TDBH ,
            TDDate: date,
            TDAddress : detailAddress,
            CLPhone: this.WtContent.LXDH || '',
            CLAddress : cl,
            rowGuid: this.RowGuid,
            BGDW: this.WtContent.BGDW || '',
            CLDD: this.WtContent.CLDD || '',
            JZDW: this.WtContent.JZDW || ''
        };

        printer(printParams,res => {
            this.setState({printed:true})
            toast('打印成功')
        });
    }

    handleImagePress = (i) => {
        let type = "choose";
        let size_w = 0;
        let size_h = 0;
        if (i == 1) {
            type = "takephoto"
        } else if (i == 2) {
            type = "choose"
        } else {
            return;
        }    

        let params = {
            type : type,
            fileType: this.photoLstTag,
            recordGuid: this.RowGuid,
            photos: this.state.photos
        }
        choosePhoto(params , data => {
             let imgs = data.imgs;
             console.log(imgs)
             if (imgs != ''){
                if (this.photoLstTag == 'cpgl'){
                    let imgItem = imgs.split(';');
                    let lst = [];
                    imgItem.map(item => {
                        if (item.length != ''){
                            lst.push({url: item , id: lst.length  , title: ''});
                        }
                    })
                    this.setState({photos: lst});
                } else {
                    let first  = imgs.substring(0,1);
                    console.log(first);
                    this.setState({car_cp:first});
                    let cityno = imgs.substring(1,2);
                    this.setState({car_city_cp:cityno})
                    let end = imgs.substring(2);
                    console.log(end);
                    this.setState({car_no:end});
                }
             }
        });
    };


    updateRecordNum  = () => {
        let params={
            UserGuid : this.props.userGuid,
            KeyWords : md5(this.props.userGuid + config.md5Secret)
        }
        this.props.GetWeiZhangTCNum(params);
    }

    /**
     * 保存 信息
     */
    saveInfo = () => {
        setConfigValue('wtfx',this.state.carDirectionValue || '');
        setConfigValue('wzdd' , this.state.detailAddress);
    }

    submit = () => {

        if (!this.checkValue()){
            return;
        }

        if (this.state.printed){
            // 已打印
            let params  = {
                RowGuid: this.RowGuid,
                TDBH: this.state.orderNo,
                HPZL: this.state.carTypeValue,
                CPHM: this.state.car_cp + this.state.car_city_cp + this.state.car_no,	
                SZLD:"",	
                RoadGuid:'',	
                WZDD: this.state.detailAddress,	
                WTFX: this.state.carDirectionValue,
                JCSJ: moment().format('YYYY-MM-DD HH:mm:ss'),
                ZFRY: this.props.userName,
                ZFRYGuid: this.props.userGuid,	
                PDRY: this.state.pariPersonName,	
                JinDu: this.JinDu,
                WeiDu: this.WeiDu,
                Password: md5(this.JinDu + config.md5Secret),
                CaseNo: this.CaseNo,
                remark: this.state.remark
            }
            Cpgl_Add(params).then(res => {
                if (res == null){
                    toast('提交成功');
                    this.updateRecordNum();
                    this.saveInfo();
                    if (this.props.navigation.state.params.reload != undefined){
                        this.props.navigation.state.params.reload();
                    }
                    this.props.naviBack();
                }
            }).catch(err => {
            })
        }else {
            toast('请先打印')
            return;
        }
    }

/**
 * 开始录音
 */
    startMicAction = () => {
        microPhotoListener({} , data => {
            let res = data.carno;
             let size = res.length;
             if (size > 1) {
                 try {
                    let car_city_cp = res.substring(0,1).toUpperCase();
                    let car_no = res.substring(1,size-1).toUpperCase();
                    this.setState({
                       car_city_cp: car_city_cp,
                       car_no: car_no
                    })
                 } catch(err) {
                    console.error(err);
                 }
             }
        });
    }

    takeCarNoAction = () => {
        this.photoLstTag = "ocr_car";
        this.ImageActionSheet.show();
    }

    chooseItemClick = (item, tag) => {
        this.photoLstTag = tag;
        this.phtotoItem = item;
        this.ImageActionSheet.show();
    };

    setBlueTooth = () => {
        setBlueTooth(res => {
            if (res.name && res.name.length > 0 ){
                this.props.setPrinter(res.name)
            }
        });
    }

    delPhotos = (item , index) => {
        let photos = this.state.photos;
        ArrayUtil.delete(photos , item);
        this.setState({photos:photos})
    }

    WZaddressShow=(x)=>{
        let str=x.indexOf('张家港市');
        console.log('index',str)
        if(str!=-1){
            let newStr = x.substring(4);
            return '张家港市'+newStr;
        } else{
            return '张家港市'+x;
        }
    }

    addressChange=(e)=>{
        if(e.length>=4){
        this.setState({
            detailAddress:e
        })
    }else{
        this.setState({
            detailAddress: '张家港市'
        })
    }
    }


    render(){
        return <ScrollView style={{ flex:1,  backgroundColor :'white'}}>
        <View style={{paddingHorizontal:15 ,flexDirection:'column' , alignItems:'center'}}>
        <View style={Styles.itemView}>
            <Text style={Styles.lableText}>贴单编号</Text>
            <TouchableOpacity style={Styles.centerView}>
                 <Text style={[Styles.rightValue]}>{this.state.orderNo}</Text>
            </TouchableOpacity>
        </View>

         {
           this.state.showRemark ? ( <View style={Styles.itemView}>
            <Text style={Styles.lableText}>派单单号</Text>
            <TouchableOpacity style={Styles.centerView}>
                 <Text style={[Styles.rightValue]}>{this.CaseNo}</Text>
            </TouchableOpacity>
        </View>) : null
       }
        
        <View style={Styles.itemView}>
            <Text style={Styles.lableText}>号牌种类</Text>
            <View style={{flex:1}}>
            <TouchableOpacity 
            onPress={()=>this.setState({carTypeVisible:true})}
            style={Styles.chooseStyle}>
                <Text style={[Styles.rightValue , {flex:1}]}>{this.state.carTypeValue}</Text>
                <Image source={require('../../image/icon_down_grey.png')} style={{width:16, height:16}}></Image>
            </TouchableOpacity>
            </View>
        </View>

        <View style={Styles.itemView}>
            <Text style={Styles.lableText}>车牌号</Text>
            <TouchableOpacity 
                onPress={()=>this.setState({showCarCity:true})}
                style={{width:24 , minHeight:30 ,  flexDirection:'row' , alignItems:'center' , justifyContent:'center'}}>
                <Text style={Styles.rightValue2}>{this.state.car_cp}</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => this.setState({showCityNo:true})}
                style={{width:24 , minHeight:30 , flexDirection:'row' , alignItems:'center' , justifyContent:'center' , backgroundColor:'#eee'}}>
                <Text style={Styles.rightCenterValue}>{this.state.car_city_cp}</Text>    
            </TouchableOpacity>
            <View style={Styles.centerView}>
                 <TextInput 
                    editable={this.state.printed ? false : true}
                    style={[Styles.rightValue2  , {flex:1 , height: 40}]}
                    value={this.state.car_no}
                    maxLength={6}
                    underlineColorAndroid="transparent"
                    onChangeText={(txt)=>this.setState({car_no:txt.toUpperCase()})}></TextInput>
            </View>
            <TouchableOpacity style={Styles.micBtn} onPress={this.startMicAction}>
                <Image source={require('../../image/icon_microphone.png')} style={{width:20 , height:24}}></Image>
            </TouchableOpacity>
            <TouchableOpacity style={Styles.rightCpBtn} onPress={this.takeCarNoAction}>
                <Text style={{color:'white'}}>拍照识别</Text>
            </TouchableOpacity>
        </View>

        <View style={Styles.itemView}>
            <Text style={Styles.lableText}>违停方向</Text>
            <View style={{flex:1}}>
            <TouchableOpacity 
            onPress={()=>this.setState({carDirectionVisible:true})}
            style={Styles.chooseStyle}>
                <Text style={[Styles.rightValue , {flex:1}]}>{this.state.carDirectionValue}</Text>
                <Image source={require('../../image/icon_down_grey.png')} style={{width:16, height:16}}></Image>
            </TouchableOpacity>
            </View>
        </View>

        <View style={[Styles.itemView]}>
            <Text style={Styles.lableText}>违章地点</Text>
            <View style={{ flex:1, borderBottomColor:'#ccc' , borderBottomWidth:1,flexDirection:'row',alignItems:'center'}}>
                <TextInput 
                multiline={true} 
                underlineColorAndroid="transparent" 
                value={this.WZaddressShow(this.state.detailAddress)} 
                style={Styles.inputAddress} 
                placeholder="详细地址" 
                onChangeText={this.addressChange}></TextInput>
            </View>

            <TouchableOpacity style={Styles.rightCpBtn} onPress={()=>this.getLocationAction()}>
                <Text style={{color:'white'}}>获取位置</Text>
            </TouchableOpacity>

        </View>


       <View style={Styles.itemView}>
            <Text style={Styles.lableText}>配对人员</Text>
            <View style={Styles.centerView}>
                <Text style={Styles.rightValue}>{this.state.pariPersonName}</Text>
            </View>
        </View>


         {
           this.state.showRemark ? (<View style={Styles.itemView}>
            <Text style={Styles.lableText}>处理意见</Text>
            <View style={{ flex:1, borderBottomColor:'#ccc' , borderBottomWidth:1}}>
            <TextInput 
            style={[Styles.rightValue  , {flex:1 }]}
            value={this.state.remark}
            underlineColorAndroid="transparent"
            onChangeText={(txt)=>this.setState({remark:txt})}
            /></View>
            </View> ) : null
       }

        <View style={Styles.itemView}>
            <Text style={Styles.lableText}>照片</Text>
            <View style={{flex:1}}></View>
        </View>
       
      
        
        <View style={{width:width , paddingHorizontal:15}}>

        <ChoosePhotoView 
            onItemClick={(item) => this.chooseItemClick(item, 'cpgl')}
            delAction={(item , index)=>this.delPhotos(item, index)}
            images={this.state.photos} 
            num={3}
            maxNum={3}
            itemheight={80}
        />
     </View>

        <View style={Styles.itemView}>
            <Text style={Styles.lableText}>蓝牙</Text>
            <View style={Styles.centerView}>
                <Text style={Styles.rightValue}>{this.props.blueToothState}</Text>
            </View>
            <TouchableOpacity style={Styles.rightBtn} onPress={this.setBlueTooth}>
               <Text style={Styles.settingTxt}>设置</Text>
            </TouchableOpacity>
        </View>

        <TouchableOpacity 
            onPress={this.printerAction}
            style={[themStyle.printBtnStyle,{ marginTop:25 }]}>
                <Text style={themStyle.whiteBtnTxtStyle}>打印</Text>
        </TouchableOpacity>

        <TouchableOpacity 
            onPress={this.submit}
            style={[themStyle.submitBtnStyle,{ marginTop:10 , marginBottom:10}]}>
                <Text style={themStyle.whiteBtnTxtStyle}>提交</Text>
        </TouchableOpacity>

        </View>
        <ChooseModal modalVisible={this.state.carTypeVisible}
            onSure={(item , index ) => {this.setState({carTypeValue:item , carTypeVisible:false})}}
            lstStr={carType}
            title="号牌种类"
            height={100}
            hideAction={()=>this.setState({carTypeVisible:false})}
        ></ChooseModal>

        <ChooseModal 
            modalVisible={this.state.carDirectionVisible}
            onSure={(item , index ) => {this.setState({carDirectionValue:item , carDirectionVisible:false})}}
            lstStr={directions}
            title="选择违停方向"
            hideAction={()=>this.setState({carDirectionVisible:false})}
        />

        <ChooseModal 
            itemStyle={{alignItems:'center'}}
            modalVisible={this.state.showCarCity}
            onSure={(item , index ) => this.setState({showCarCity:false , car_cp:item})}
            lstStr={carCity}
            title='车牌归属地'
            numColumns={4}
            hideAction={()=>this.setState({showCarCity:false})}
        />

        <ChooseModal 
            itemStyle={{alignItems:'center'}}
            modalVisible={this.state.showCityNo}
            onSure={(item)=> this.setState({showCityNo:false , car_city_cp:item})}
            lstStr={carCityNo}
            numColumns={6}
            hideAction={()=>this.setState({showCityNo:false})}
        />

         <ActionSheet
                ref={o => this.ImageActionSheet = o}
                title="选择图片"
                options={imageOptions}
                cancelButtonIndex={CANCEL_INDEX}
                onPress={this.handleImagePress}/>
        </ScrollView>
    }
}   

const mapStateToProps = state => ({
    userName : state.main.userName,
    blueToothState: state.main.printerName,
    userGuid: state.main.UserGuid,
    depCode: state.main.DeptCode
});

const mapDispatchToProps = dispatch => ({
    naviPush: (routeName, params) => {
        dispatch(naviPush(routeName, params));
    },
    naviBack: (routeName) => {
        dispatch(naviBack(routeName))
    },
    setPrinter: (name) => {
        dispatch(setPrinter(name));
    },
    GetWeiZhangTCNum: (params) => {
        dispatch(GetWeiZhangTCNum(params))
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Cpgl);
