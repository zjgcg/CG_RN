import {StyleSheet} from 'react-native';
import themeConfig from '../../config/theme'

 const Styles = StyleSheet.create({

    itemView: {
        flexDirection:'row' , 
        alignItems:'center' , 
        minHeight:48
    },
    centerView: {
        flex:1 , borderBottomColor:'#ccc' , borderBottomWidth:1, height:40, flexDirection:'row' , alignItems:'center'
    },
    chooseStyle: {
        width:160 , backgroundColor:'#ebebeb' , borderColor:'#cccccc', 
        borderRadius:3 ,height:40 ,paddingHorizontal:10 , 
        flexDirection:'row' , alignItems:'center'
    },
    lableText:{
        fontSize:themeConfig.commonSize , 
        color:themeConfig.grey1Color ,
        width: 80,
    },
    rightValue : {
        color: themeConfig.blue2Color,
        fontSize: themeConfig.normalSize,
        marginRight:10,
    },
    rightValue2 : {
        color: themeConfig.blue2Color,
        fontSize: themeConfig.normalSize,
        marginRight:3,
    },
    rightCenterValue: {
        color: themeConfig.blue2Color,
        fontSize: themeConfig.normalSize,
    },
    rightHint: {
        color: themeConfig.blue2Color,
        fontSize: themeConfig.normalSize,
        marginRight:10
    },
    rightBtn: {
        flexDirection:'row' ,
        justifyContent:'center' , 
        alignItems:"center",
        width:60,
        height: 36,
        backgroundColor:themeConfig.blue1Color , 
        borderRadius:3,
        marginLeft:5
    },
    micBtn: {
        flexDirection:'row' ,
        justifyContent:'center' , 
        alignItems:"center",
        width:40,
        height: 36,
        backgroundColor:themeConfig.blue1Color , 
        borderRadius:3,
        marginLeft:5
    },
    rightCpBtn : {
        flexDirection:'row' ,
        justifyContent:'center' , 
        alignItems:"center",
        width:80,
        height: 36,
        backgroundColor:themeConfig.blue1Color , 
        borderRadius:3,
        marginLeft:5
    },
    rightBtnTxt: {
        color:'white' ,
        fontSize:themeConfig.normalSize , 
    },

    inputAddress: {
        color: themeConfig.blue2Color,
        fontSize: themeConfig.commonSize,
        flex:1
    },

    settingTxt: {
        color:'white', 
        fontSize: themeConfig.commonSize
    },

    warnText: {
        color: 'red',
        fontSize: themeConfig.commonSize
    }   

});

module.exports = Styles;
