import NetUtil from '../../common/NetUtil';
import config from '../../config/config';

export const GetMessageList = (params) => {
    let url = '/GetMessageList'
    return NetUtil.postNew(config.apiUrl + url , params);
}