'use strict';
import React from 'react';
import {naviPush} from '../../navigatorApp/actions';
import {View , Text , FlatList ,Image ,TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import NavBarCommon from '../../component/NavBarCommon'
import msStyle from './styles'
import {GetMessageList} from './actions'
import { getConfig } from '../../utils/NativeUtil';
import config from '../../config/config'
import themeconfig from '../../config/theme';
import dwstyles from '../dwjs/styles'
const md5 = require('md5');

const page = 0;
let hasMore = false;
class Message extends React.Component {

    constructor(props){
        super(props);
        this.state={
            data:[],
            isLoading: true,
            refreshing:false,
            flag:true
        },
        this._data=[]
    }

    componentDidMount(){
        page=0;
        this.getData();
    }

    reloadData = () => {
        console.log('reload')
        page=0;
        this.getData();
    }

    getData=()=>{
        var params={}
        // params={
        //     Userguid: this.props.userGuid,
        //     KeyWords: md5(res.userguid + config.md5Secret),
        //     PageIndex: page
        // }
        getConfig('userguid',res=>{
            console.log(res)
            params={
                Userguid: res.userguid,
                KeyWords: md5(res.userguid + config.md5Secret),
                PageIndex: page
            }
            console.log('params',params)
        GetMessageList(params).then(res=>{
            console.log(res)
            if (res.length < 10) {
                hasMore = false
            } else {
                hasMore = true
            }
            if (page == 0) {
                this._data = res;
            } else {
                this._data = this._data.concat(res);
            }
            for (var i=0;i<this._data.length;i++){
                this._data[i].contentShow = true;
            }
            this.setState({
                data: this._data,
                isLoading: false,
                refreshing: false,
            })
            console.log('data',this.state.data);
            console.log('this._data',this._data)
        }).catch(msg=>{
            console.log(msg)
        })
        })
    }

    onEndReached = () => {
        console.log('hasMore');
        console.log(hasMore)
        if (hasMore) {
            page++;
            this.getData()
        }
    }

    onRefresh = () => { //刷新回调函数
        this.setState({
            refreshing: true,
            isLoading: true
        })
        page = 0;
        this.getData()
    }

    _loading=()=>{
        return(
            <View style={dwstyles.footerimg}>
            <Text>isLoading...</Text>
            </View>
        )
    }

    _loaded=()=>{
        return (
            <View style={dwstyles.footerimg}>
                <Text>已经到底了</Text>
            </View>
        )
    }


    imageshow=(index)=>{
        console.log('index',index);
        console.log(this.state.data[index].contentShow)
        if(this.state.data[index].contentShow){
        return (
            <Image source={require('../../image/icon_down.png')} style={{width: 25 ,height : 25}}/>
        )
        }
        else{
            return (
            <Image source={require('../../image/icon_up.png')} style={{width: 25 ,height : 25}}/>
            )
            }
    }
    handleClick=(i)=>{
        console.log(i);
        let data=this.state.data; 
        data[i].contentShow=!data[i].contentShow; 
        this.setState({data:data});
        console.log('newdata',this.state.data[i].contentShow);
    }



    _renderItem=({item,index})=>{
        if(item.IsHandle=='0'){
        return(
            <View style={msStyle.totalbox}>
                <View style={msStyle.header}>
                    <Text style={msStyle.header_t1}>未处理</Text>
                    <Text style={msStyle.header_t2} numberOfLines={1} ellipsizeMode='tail' >{item.CaseNo}</Text>
                    {/* <TouchableOpacity onPress={(i)=>{i=index;this.handleClick(i)}}>
                    {this.imageshow(index)}
                    </TouchableOpacity> */}
                </View>
                <View style={msStyle.line} />
                <View style={[msStyle.content,{display:this.state.data[index].contentShow?'flex':'none'}]}>
                    <Text style={msStyle.content_t1}>{item.CaseDate}</Text>
                    <Text style={msStyle.content_t2}>{item.CaseInfo}</Text>
                    <View style={msStyle.cpbox}>
                        <TouchableOpacity style={msStyle.cp} onPress={()=>{this.props.naviPush('Cpgl',{CaseNo: item.CaseNo , fromType:'message' , reload: this.reloadData})}}><Text style={msStyle.cp_text}>违停抄牌</Text></TouchableOpacity>
                        <TouchableOpacity style={[msStyle.cp,{marginLeft:10}]} onPress={()=>{this.props.naviPush('CommonCase',{CaseNo: item.CaseNo,rowguid:item.RowGuid , reload: this.reloadData })}}><Text style={msStyle.cp_text}>一般案件</Text></TouchableOpacity>
                        <TouchableOpacity style={[msStyle.cp,{marginLeft:10}]} onPress={()=>{this.props.naviPush('Program' , {CaseNum: item.CaseNo , fromType:'message' , reload: this.reloadData})}}><Text style={msStyle.cp_text}>简易程序</Text></TouchableOpacity>
                        <TouchableOpacity style={[msStyle.cp,{marginLeft:10}]} onPress={()=>{this.props.naviPush('NoHandle',{CaseNo:item.CaseNo,rowguid:item.RowGuid , reload: this.reloadData});}}><Text style={msStyle.cp_text}>无需处理</Text></TouchableOpacity>
                    </View>
                </View>
                <View style={[msStyle.line,{marginTop: -6}]} />
            </View>
        )
        }
        else{
            const handleType=(x)=>{
                let title=''
                switch(x){
                    case '01' : title='违停抄牌';break;
                    case '02' : title='一般案件';break;
                    case '03' : title='简易程序';break;
                    case '04' : title='无需处理';break;
                }
                return title;
            }
            return(
                <View style={msStyle.totalbox}>
                <View style={msStyle.header}>
                    <Text style={[msStyle.header_t1,{backgroundColor:'lightgreen'}]}>已处理</Text>
                    <Text style={msStyle.header_t2} numberOfLines={1} ellipsizeMode='tail'>{item.CaseNo}</Text>
                    {/* <Image source={require('../../image/icon_down_grey.png')} style={{width: 25 ,height : 25}}/> */}
                    {/* <TouchableOpacity onPress={(i)=>{i=index;this.handleClick(i)}}>
                    {this.imageshow(index)}
                    </TouchableOpacity> */}
                </View>
                <View style={msStyle.line} />
                <View style={msStyle.content}>
                    <Text style={msStyle.content_t1}>{item.CaseDate}</Text>
                    <Text style={msStyle.content_t2}>{item.CaseInfo}</Text>
                    <View style={msStyle.cpbox}>
                        <TouchableOpacity style={[msStyle.cp,{backgroundColor:themeconfig.grey3Color}]}><Text style={msStyle.cp_text}>{handleType(item.HandleType)}</Text></TouchableOpacity>
                    </View>
                </View>
                <View style={[msStyle.line,{marginTop: -6}]} />
            </View>
            )
        }
    }

    _keyExtractor = (item, index) => index;

    render(){
        return <View>
             <NavBarCommon  
                    customStyle={{
                        borderBottomWidth: 0,
                        backgroundColor: 'white'
                    }}
                    rightTitle=""
                    title="消息"></NavBarCommon>
                    <View style={{width:'100%',height: 10}}/>
            <FlatList data={this.state.data} renderItem={this._renderItem} keyExtractor={this._keyExtractor} ListFooterComponent={this.state.isLoading ? this._loading:this._loaded} onEndReached={this.onEndReached} onEndReachedThreshold={0} onRefresh={this.onRefresh} refreshing={this.state.isLoading}/>
        </View>
    }
}

const mapStateToProps = state => ({
    userGuid: state.main.UserGuid
});

const mapDispatchToProps = dispatch => ({
    naviPush: (routeName, params) => {
        dispatch(naviPush(routeName, params));
    },
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Message);