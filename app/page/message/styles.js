import {StyleSheet} from 'react-native';
import themeconfig from '../../config/theme';

const msStyle= StyleSheet.create({
    totalbox:{
        flexDirection:'column',
        paddingHorizontal : 22
    },
    header:{
        width:'100%',
        height:35,
        flexDirection:'row',
        marginTop: 10,
        alignItems:'center'
    },
    line:{
        width:'100%',
        height: 1,
        backgroundColor: themeconfig.grey3Color
    },
    header_t1:{
        width : '16%',
        height : 24 ,
        backgroundColor: themeconfig.red1Color,
        textAlign:'center',
        lineHeight: 20 ,
        color: '#fff',
        borderRadius: 3
    },
    header_t2:{
        marginLeft: 10,
        width: '75%'
    },
    content:{
        width:'100%',
        flexDirection:'column',
        minHeight: 130
    },
    content_t1:{
        fontSize: 13,
        color: themeconfig.blue2Color,
        marginTop: 12,
    },
    content_t2:{
        fontSize: 13,
        color: themeconfig.grey2Color,
        marginTop: 12,
        width: '98%'
    },
    cpbox:{
        width:'100%',
        flexDirection:'row',
        // justifyContent:'center',
        // alignItems:'center',
        marginTop: 20,
    },
    cp:{
        width: '22%',
        height: 32,
        borderRadius: 3,
        backgroundColor: themeconfig.blue1Color,
        justifyContent:'center',
        alignItems:'center'
    },
    cp_text:{
        color:'white'
    }
})

module.exports=msStyle;