import {
    StyleSheet
} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import themeconfig from '../../config/theme'

const styles = StyleSheet.create({

    itemView :{
        flexDirection:'row' ,
        justifyContent:'space-between' , 
        alignItems:"center",
        paddingHorizontal:15,
    },
    lable: {
        color: '#666666',
        fontSize: themeconfig.commonSize
    }

});

export default  styles;