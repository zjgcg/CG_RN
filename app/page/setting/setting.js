'use strict';
import React from 'react';
import {View , Text , TouchableOpacity} from 'react-native';
import {naviPush} from '../../navigatorApp/actions';
import {setUserGuid,setUserName , setUserTeam ,} from '../main/actions';
import {connect} from 'react-redux';
import Styles from './styles';
import InputView from '../compents/InputView';
import NavBarCommon from '../../component/NavBarCommon'
import {GetDepartUserInfo,GetWeiZhangTCNum} from '../home/actions'
import {setPrinter} from '../main/actions';
import { getConfig  , setBlueTooth} from '../../utils/NativeUtil';
import themStyle  from  '../../config/themeStyle';
class Setting extends React.Component {

    static navigationOptions = ({navigation, screenProps}) => {
        return null;
    };


    constructor(props){
        super(props);
        this.state = {
            pariState:'',
            departmentCode:''
        }
    }

    componentWillMount(){

        getConfig('pariState',res=>{
            console.log(res);
            if(res.pariState==''){
                this.setState({
                    pariState:'未配对'
                })
            }else{
            this.setState({
                pariState:res.pariState
            })
        }
        })

        getConfig('userguid' , res => {
            console.log(res);
            let params={
                UserGuid:res.userguid
            }
            GetDepartUserInfo(params).then(res2=>{
                console.log('info',res2)
                console.log(res2[0].DeptCode)
                this.setState({
                    departmentCode:res2[0].DeptCode
                })
            }).catch(msg=>{
                console.log(msg)
            })
        })
    }


    setBlueToothAction = () => {
        setBlueTooth(res => {
            console.log(res);
            if (res.name && res.name.length > 0 ){
                this.props.setPrinter(res.name)
            }
        });
    }

    logoutAction = () => {
        console.log(this.props.UserGuid)
        this.props.setUserGuid('')
        this.props.setUserName('未登录')
        this.props.setUserTeam('')
        this.props.naviPush('Login',{})
    }

    render(){
        return <View>
            <NavBarCommon  
                    customStyle={{
                        borderBottomWidth: 0,
                        backgroundColor: 'white'
                    }}
                    rightTitle=""
                    title="设置"></NavBarCommon>
                  
            <InputView 
                leftValue="队员配对"
                rightValue={this.state.pariState}
                rightHint={this.state.pariState}
                itemClickAction={()=>this.props.naviPush("DWJS2" , {departmentCode:this.state.departmentCode, teaminfo:'' , abovelevel:''})}
            ></InputView>
            
            <InputView 
                leftValue="密码修改"
                rightValue=""
                rightHint=""
                itemClickAction={()=>alert('功能暂未开通')}
            ></InputView>

            <InputView 
                leftValue="版本更新"
                rightValue=""
                rightHint=""
                rightBtnValue="更新"
                rightBtnAction={()=>alert('您已经是最新版本')}
            ></InputView>

            <InputView 
                leftValue="蓝牙打印机"
                rightValue={this.props.printerName}
                rightHint="请连接"
                itemClickAction={()=> this.setBlueToothAction() }
            ></InputView>

            <View style={{flexDirection:'row' , justifyContent:'center'}}>
           <TouchableOpacity 
            onPress={this.logoutAction}
            style={[themStyle.printBtnStyle,{ marginTop:25 ,  }]}>
                <Text style={themStyle.whiteBtnTxtStyle}>退出登录</Text>
            </TouchableOpacity>
            </View>
        </View>
    }
}

const mapStateToProps = state => ({
     printerName: state.main.printerName,
     UserGuid: state.main.UserGuid
});

const mapDispatchToProps = dispatch => ({
    naviPush: (routeName, params) => {
        dispatch(naviPush(routeName, params));
    },
    setPrinter: (name) => {
        dispatch(setPrinter(name));
    },
    setUserGuid: (userguid) => {
        dispatch(setUserGuid(userguid));
    },
    setUserName: (name) => {
        dispatch(setUserName(name));
    },
    setUserTeam: (team) => {
        dispatch(setUserTeam(team));
    },
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Setting);