import React,{Component} from 'react';
import {View,Text,ScrollView,TouchableOpacity,Image,TextInput} from 'react-native';
import CLNavigatorHeader from '../../component/CLNavigatorHeader';
import {connect} from 'react-redux';
import { getConfig,choosePhoto} from '../../utils/NativeUtil';
import Styles from '../cpgl/styles';
import styles from '../program/styles';
import themeconfig from '../../config/theme';
import ChoosePhotoView from '../compents/ChoosePhotoView';
import ActionSheet from 'react-native-actionsheet';
import BigButton from '../../component/BigButton'
import { ArrayUtil } from '../../utils/common';
import {Message_Update} from './actions'
import config from '../../config/config';
import {getRandOrderNo} from '../cpgl/actions'
const md5 = require('md5');
import {naviPush , naviBack} from '../../navigatorApp/actions';
const CANCEL_INDEX = 0;
const imageOptions = ['取消', '拍照上传', '选择图片'];
class CommonCase extends Component {
    static navigationOptions = ({ navigation, screenProps }) => ({
        header: <CLNavigatorHeader navigation={navigation} title="一般案件"/>
    });

    constructor(props){
        super(props);
        this.state={
            orderNo:'自动生成',
            orderNo2:'自动生成',
            ptname:'',
            detail:'',
            tjzp: [], //添加照片
        }
        this.RowGuid=''
    }

    chooseItemClick = (item, tag) => {
        this.photoLstTag = tag;
        this.phtotoItem = item;
        this.ImageActionSheet.show();
    };

    handleImagePress = (i) => {
        let type = "choose";
        let size_w = 0;
        let size_h = 0;
        if (i == 1) {
            type = "takephoto"
        } else if (i == 2) {
            type = "choose"
        } else {
            return;
        }
        
        let params = {
            type : type,
            fileType: 'ybaj',
            recordGuid: this.RowGuid
        }
        choosePhoto(params , data => {
             let imgs = data.imgs;
             if (imgs != ''){
                let lst = this.state.tjzp;
                lst.push({url: imgs , id: lst.length  , title: ''});
                this.setState({
                    tjzp: lst
                });
             }
        });
    };

    getRandOrderNoAction  = () => {
        if (this.state.orderNo == '自动生成'){
            getRandOrderNo(this.props.userGuid).then(res => {
                console.log(res)
                let data = res[0];
                this.RowGuid = data.RowGuid;
            }).catch(msg => {
                console.error(msg)
            });
        }
    }

    delPhotos = (item , index) => {
        console.log(item)
        console.log(index)
        let photos = this.state.tjzp;
        ArrayUtil.delete(photos , item);
        this.setState({tjzp:photos})
    }

    componentDidMount(){
        this.getRandOrderNoAction();
        getConfig('pariState',res=>{
            console.log(res.pariState);
            this.setState({
            orderNo: this.props.navigation.state.params.CaseNo,
            ptname: this.props.userName+','+res.pariState
        })
    });
    }

    submit=()=>{
        let params={
            RowGuid : this.props.navigation.state.params.rowguid,
            UserGuid : this.props.userGuid,
            Password : md5(this.props.userGuid + config.md5Secret),
            Remark : this.state.detail,
            type : '02'
        }
        console.log('params',params)
            Message_Update(params).then(res=>{
                console.log(res);

                if (this.props.navigation.state.params.reload != undefined){
                    this.props.navigation.state.params.reload();
                }
                toast('提交成功')
                this.props.naviBack();

            }).catch(msg=>{
                console.log(msg)
            })
    }
    render(){
        return (
            <View style={styles.totalbox}>
                <View style={Styles.itemView}>
                    <Text style={Styles.lableText}>派单号</Text>
                    <TouchableOpacity style={Styles.centerView} onPress={this.getRandOrderNoAction}>
                        <Text style={[Styles.rightValue]}>{this.state.orderNo}</Text>
                    </TouchableOpacity>
                </View>
                <View style={Styles.itemView}>
                    <Text style={Styles.lableText}>处罚编码</Text>
                    <TouchableOpacity style={Styles.centerView} onPress={this.getRandOrderNoAction}>
                        <Text style={[Styles.rightValue]}>{this.state.orderNo2}</Text>
                    </TouchableOpacity>
                </View>
                <View style={Styles.itemView}>
                <Text style={Styles.lableText}>配对队员</Text>
                    <TextInput 
                    value={this.state.ptname} 
                    onChangeText={(txt)=>this.setState({ptname:txt})}
                    style={[styles.t1,{color:themeconfig.blue2Color}]} placeholder='郭德纲，于谦' placeholderTextColor={themeconfig.blue2Color}/>
                </View>
                <View style={Styles.itemView}>
                <Text style={Styles.lableText}>详细描述</Text>
                    <TextInput 
                    value={this.state.detail} 
                    onChangeText={(txt)=>this.setState({detail:txt})}
                    style={[styles.t1,{color:themeconfig.blue2Color}]} multiline={true} numberOfLines={5} underlineColorAndroid='transparent'/>
                </View>
                <View style={[Styles.itemView,{flexDirection:'column',alignItems:'flex-start',marginTop: 10}]}>
                    <Text style={Styles.lableText}>添加照片</Text>
                    <View style={{width:'100%',height:60,marginTop: 15,flexDirection:'row'}}>
                    <ChoosePhotoView 
                    onItemClick={(item) => this.chooseItemClick(item, 'ybaj')}
                    delAction={(item , index)=>this.delPhotos(item, index)}
                    images={this.state.tjzp}
                    num={3}
                    maxNum={3}
                    itemheight={80}
                />
         
                <ActionSheet
                ref={o => this.ImageActionSheet = o}
                title="选择图片"
                options={imageOptions}
                cancelButtonIndex={CANCEL_INDEX}
                onPress={this.handleImagePress}/>
                    </View>
                </View>
                <BigButton label='提交' backgroundColor={themeconfig.blue1Color} style={{marginTop: 55}} onPress={this.submit}/>
            </View>
        )
    }
}

const mapStateToProps = state => ({
    userName: state.main.userName,
    userGuid: state.main.UserGuid,
});

const mapDispatchToProps = dispatch => ({
    naviPush: (routeName, params) => {
        dispatch(naviPush(routeName, params));
    },
    naviBack: (routeName) => {
        dispatch(naviBack(routeName))
    },
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CommonCase);