import NetUtil from '../../common/NetUtil';
import config from '../../config/config';

export const Message_Update = (params) => {
    const  method = "/Message_Update";
    return NetUtil.postNew(config.apiUrl + method , params);
}
