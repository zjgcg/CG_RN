import NetUtil from '../../common/NetUtil';
import config from '../../config/config';

export const login = (params ) => {
    let url = '/Login'
    return NetUtil.postNew(config.apiUrl + url , params);
}


export const bindClient = (params) => {
    let url = '/UpdateClientId';
    return NetUtil.postNew(config.apiUrl + url , params);
}


export const checkVersion  = (params) => {
    let url = '/getVersion';
    return NetUtil.postNew(config.apiUrl + url , params);
}