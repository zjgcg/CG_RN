import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    titleStyle: {
        color:'white'
    }
});

module.exports = styles;