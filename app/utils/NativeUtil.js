'use strict';

import {
    NativeModules,
} from 'react-native';


export const configkey = {
    LoginId:'loginid',
    Password: 'password'
};

export const wxShare = (title, description, image, url, scene) => {
    NativeModules.RNJsBridge.invoke("wxshare", {title: title, description: description, image: image, url: url, scene: scene}, () => {
        
    })
}

export const aliPay = (payString , callback) => {
    NativeModules.RNJsBridge.invoke("alipay", {payString: payString}, () => {
        callback();
    })
};

export const wxPay = (partnerid, prepayid, packageName, noncestr, timestamp, sign) => {
    NativeModules.RNJsBridge.invoke("wxpay", {partnerid: partnerid, prepayid: prepayid, package: packageName, noncestr: noncestr, timestamp: timestamp, sign: sign}, () => {
    })
};

export const getConfig = (keys, callback) => {
    NativeModules.RNJsBridge.invoke("getconfigvalue", {keys: keys}, (params) => {
        callback(params);
    });
};

export const setConfigValue = (key, value) => {
    console.log('setconfigvalue')
    NativeModules.RNJsBridge.invoke("setconfigvalue", {key: key, value: value}, (res) => {
    })
};

// 最近联系人
export const Navi_RecentContact = () => {
    console.log('Navi_RecentContact')
    NativeModules.RNJsBridge.invoke("im_recentcontact", {}, (params) => {
    })
};

// IM登录
export const IM_Login = (params) => {
    console.log('im_login')
    NativeModules.RNJsBridge.invoke("im_login", params, (params) => {
    })
};

// 单聊
export const IM_ChatOne = (account, firstWord) => {
    console.log('IM_ChatOne')
    NativeModules.RNJsBridge.invoke("im_chatone", {account: account, firstWord: firstWord}, (params) => {
    })
};

export const gotoBaidu = (lat, lng) => {
    console.log('gotoBaidu')
    NativeModules.RNJsBridge.invoke("gotoBaidu", {lat: lat, lng: lng}, (params) => {
    })
};

export const showLoading = (value) => {
    console.log('showLoading')
    NativeModules.RNJsBridge.invoke('showloading', {info: value}, (res) => {
    })
}

export const hideLoading = (value) => {
    console.log('hideLoading')
    NativeModules.RNJsBridge.invoke('hideloading', {}, (res) => {
    })
}

export const toast = (value) => {
    console.log('toast' , value || '')
    NativeModules.RNJsBridge.invoke('showinfo', {info: value}, (res) => {
    });
};


/**
 * 上传图片
 */

export const choosePhoto = (params, callback) => {
    NativeModules.RNJsBridge.invoke('cgPhotos', params, function (res) {
        callback(res);
    });
};

export const chooseCity = (callback) => {
    console.log('choosecity')
    NativeModules.RNJsBridge.invoke('chooseCity', {}, function (res) {
        callback(res);
    });
};

export const checkCity = (callback) => {
    console.log('checkcity');
    NativeModules.RNJsBridge.invoke("checkcity", {}, (params) => {
        callback(params);
    });
};

export const getLocation = (callback) => {
    console.log('getlocation')
    NativeModules.RNJsBridge.invoke("getlocation", {}, (values) => {
        callback(values);
    });
};

export const unRead = (callback) => {
    console.log('unRead')
    NativeModules.RNJsBridge.invoke("unRead", {}, (values) => {
        callback(values);
    });
};

/**
 * 地图上选点
 */
export const getMapLocation = (params, callback) => {
    console.log('getMapLocation')
    NativeModules.RNJsBridge.invoke('chooseMapLocation', params, values => {
        callback(values);
    });
};

/**
 * 导航
 */
export const navi = (params) => {
    console.log('bd_navi')
    NativeModules.RNJsBridge.invoke('bd_navi', params, values => {
        callback(values);
    });
};

export const setBlueTooth = (callback) => {
    NativeModules.RNJsBridge.invoke("setBlueThooth",{},value=>{
        callback(value)
    });
}


export const printer = (params , callback) => {
    NativeModules.RNJsBridge.invoke("printerCpgl" , params , value => {
        callback(value);
    })
}

export const printerProgram = (params , callback) => {    
    NativeModules.RNJsBridge.invoke("printerJycx" , params , value => {
        callback(value);
    });
}

export const checkContectState = (params , callback) => {
    NativeModules.RNJsBridge.invoke("checkPrinterState" , params , value => {
         callback(value)
    })
}


export const contectPrinter = (params , callback) => {
    NativeModules.RNJsBridge.invoke("contectPrinter" , params , value => {
        callback(value)
    })
}


export const updateApp = (params , callback) => {
    NativeModules.RNJsBridge.invoke("updateApp" , params , value => {
        callback(value)
    })
}

export const microPhotoListener = (params , callback) => {
    NativeModules.RNJsBridge.invoke("microPhotoListener" , params , value => {
        callback(value)
    });
}
