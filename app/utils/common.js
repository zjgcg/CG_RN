/**
 * Created by jf on 16/1/14.
 */
import config from '../config/config'
import sj from '../image/sj.png'

class StringUtil {
    static replaceAll(s, s1, s2) {
        return s.replace(new RegExp(s1, "gm"), s2);
    }

    static overToDecimal2(x) {
        let s = x.toString();
        s = s.replace(/[^\d.]/g, "").replace(/^\./g, "").replace(/\.{2,}/g, ".").replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
        let f1 = parseFloat(s);
        if (isNaN(f1) || f1 == 0) {
            return s;
        }
        
        let rs = s.indexOf('.');
        if (rs == s.length - 1) {
            return s;
        }
        let f = Math.floor(f1*100)/100;
        return f.toString();
    }

    static allToDecimal2(x) {
        let f1 = parseFloat(x);
        if (isNaN(f1) || f1 == 0) {
            return x;
        }
        
        let f = Math.round(x*100)/100;
        let s = f.toString();
        let rs = s.indexOf('.');
        if (rs < 0) {
            rs = s.length;
            s += '.';
        }
        while (s.length <= rs + 2) {
            s += '0';
        }
        return s;
    }
}

class ArrayUtil {
    static delete(arr, val) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == val) {
                arr.splice(i, 1);
                break;
            }
        }
    }

    static Contain(item, array) {
        let contain = false;
        array.map((key) => {
            if (item == key) {
                contain = true
            }
        });
        return contain;
    };
}

class ImageUtil {
    static getImage (url,defaultImage) {
        if (url != undefined){

            let imgSource = {
                uri: url
            };

            if (url == ""|| url == null){
                if (defaultImage == undefined){
                    imgSource = sj
                }else{
                    imgSource = defaultImage
                }
                
            }

            if (url.indexOf("http") != -1){
                imgSource = {uri:url}
            }
            return imgSource
        }else {
            return {uri:""};
        }
    }

    static getFinalImageUrl (url) {
        let realUrl = "";
        if (url != undefined){

            realUrl = config.img + url

            if (url.indexOf("http") != -1){
                realUrl = url
            }
            return realUrl
        }else {
            return "";
        }
    }
}

export {StringUtil, ArrayUtil ,ImageUtil};
