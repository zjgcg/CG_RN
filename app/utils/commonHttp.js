import NetUtil from '../common/NetUtil';
import config from '../config/config';

export const getQiniuToken = (success, error) => {
    const params = {};
    let url = config.apiUrl + "/Qiniu";
    NetUtil.post(url, params, (res) => {
        console.log(res);
        success(res[0]);
    });
};

export const UpdateClientId = (userguid, clientid) => {
    let url = config.apiUrl + "/UpdateClientId";
    let params = {UserGuid: userguid, ClientId: clientid}
    console.log(params);
    NetUtil.post(url, params, (res) => {

    })
}
