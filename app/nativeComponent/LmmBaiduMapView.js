import {requireNativeComponent,View,ViewPropTypes} from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';

class MapView extends React.Component {

    static propTypes = {
        ...ViewPropTypes,
        style: ViewPropTypes.style,
        center: PropTypes.shape({lat: PropTypes.number.isRequired, lng: PropTypes.number.isRequired}),
      };

    render() {
        return <LmmBaiduMapView {...this.props}/>;
    }
}

const LmmBaiduMapView = requireNativeComponent('LmmBaiduMap', MapView);

// requireNativeComponent automatically resolves 'RNTMap' to 'RNTMapManager'
module.exports = MapView;
