/**
 * Created by xmgong on 2017/10/29.
 * 输入框的搜索bar
 */

import React, {
    Component
} from 'react';
import {
    View,
    TextInput,
    StyleSheet,
    Image,
} from 'react-native';

import {width, height, totalSize} from 'react-native-dimension';

const InputHeight = 28

class SearchBarInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKey: ''
        }
    }

    //触发搜索
    searchClick = () => {
        this.props.onSearchPress(this.state.searchKey)
    };


    render() {

        let hintStr = '请输入行业，区域或品牌名称';

        if (this.props.hintStr.length > 0) {
            hintStr = this.props.hintStr;
        }

        return (<View style={[styles.searchBtn, {backgroundColor: "#ffffff"}]}>
            <Image style={{width: 20, height: 20}} source={require('../image/search.png')}/>
            <TextInput style={{fontSize: 15, color: "#cdcdcd", marginLeft: 5, flex: 1}} placeholder={hintStr}
                       value={this.state.searchKey}
                       returnKeyLabel='search'
                       multiline={false}
                       underlineColorAndroid='transparent'
                       returnKeyType="search"
                       onChangeText={(txt) => {
                           this.setState({searchKey: txt})
                       }}
                       onSubmitEditing={() => this.searchClick()}
            />
        </View>)

    }
}

const styles = StyleSheet.create({
    searchBtn: {
        flex:1,
        paddingLeft: 10,
        height: InputHeight,
        flexDirection: "row",
        backgroundColor: "#cccccc",
        alignItems: "center"
    },
});

export default SearchBarInput;

