import React,{Component} from 'react';
import {View,TouchableOpacity,Text,StyleSheet , Dimensions} from 'react-native';
import themeConfig from '../config/theme'
const {width , height} = Dimensions.get('window');
class BigButton extends Component {
    render(){
        return(
            <View style={styles.bt_view}>
            <TouchableOpacity 
                onPress={this.props.onPress}
            style={[styles.submit,{backgroundColor: this.props.backgroundColor},this.props.style]}>
            <Text style={styles.btnTxtStyle}>{this.props.label}</Text>
            </TouchableOpacity></View>
        )
    }
}

const styles= StyleSheet.create({
    bt_view:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },
    submit:{
        width: width - 60,
        height: 40,
        backgroundColor: '#eeeeee',
        flexDirection:'row' , 
        alignItems:'center' , 
        justifyContent:'center',
        borderRadius:3 
    },
    btnTxtStyle: {
        fontSize : themeConfig.titleSize,
        color : '#fff'
    }, 
})

export default BigButton;
