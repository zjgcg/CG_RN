import React, {Component} from 'react';
import {
    View,
    Image,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    Text
} from 'react-native';
let {width, height} = Dimensions.get('window')

import LmmImageView from './LmmImageView'

import Swiper from 'react-native-swiper';
import themeconfig from '../config/theme'

const CLImagesView = ({images, size, onItemClick}) => {
    return <Swiper autoplay={true}
                paginationStyle={{ bottom:5 }}
                   key={images.length}
                   autoplayTimeout={5}
                   dot={<View
                       style={{
                           backgroundColor: 'rgba(0,0,0,0.3)',
                           width: 8,
                           height: 8,
                           borderRadius: 4,
                           marginLeft: 3,
                           marginRight: 3,
                           marginTop: 3,
                           marginBottom: 3,
                       }}
                   />}
                   activeDot={<View
                       style={{
                           backgroundColor: 'white',
                           width: 8,
                           height: 8,
                           borderRadius: 4,
                           marginLeft: 3,
                           marginRight: 3,
                           marginTop: 3,
                           marginBottom: 3,
                       }}
                   />}>
        {images
            .map((item, i) => {
                return <TouchableOpacity
                    onPress={() => onItemClick(i)}
                    key={i}
                    style={{flex: 1}}>
                    <LmmImageView
                        style={size}
                        source={{
                            uri: item,
                        }}
                    />
                </TouchableOpacity>
            })}
    </Swiper>
};

export default CLImagesView;
