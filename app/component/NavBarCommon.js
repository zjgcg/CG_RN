import React, {
  Component
} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Platform,
  TouchableOpacity
} from 'react-native';

import themeconfig from '../config/theme'
import {naviHeight, paddingTop} from '../utils/ScreenUtil'


class NavigationBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    // leftTitle和leftImage 优先判断leftTitle (即 文本按钮和图片按钮优先显示文本按钮)
    const {
      title,
      leftTitle,
      leftImage,
      leftAction,
      rightTitle,
      rightImage,
      rightImage2,
      rightAction,
      titleStyle,
      rightAction2,
    } = this.props;
    return (
      <View style={[styles.barView, this.props.customStyle]}>
              <View style={ styles.showView }>
                {
                        leftTitle
                        ?
                        <TouchableOpacity style={styles.leftNav} onPress={ ()=>{leftAction()} }>
                            <View style={{alignItems: 'center'}}>
                                <Text style={styles.barButton}>{leftTitle}</Text>
                            </View>
                        </TouchableOpacity>
                        :
                        (
                            leftImage
                            ?
                            <TouchableOpacity style={styles.leftNav} onPress={ ()=>{leftAction()} }>
                                <View style={{alignItems: 'center'}}>
                                    <Image source={ leftImage }/>
                                </View>
                            </TouchableOpacity>
                            : null
                        )
                }
                {
                        title ?
                        <Text style={[styles.title,titleStyle]}>{title || ''}</Text>
                        : null
                }
                {
                        rightTitle ?
                        <TouchableOpacity style={styles.rightNav} onPress={ ()=>{rightAction()} }>
                            <View style={{alignItems: 'center'}}>
                              <Text style={styles.barButton}>{rightTitle}</Text>
                            </View>
                        </TouchableOpacity>
                  : (rightImage ?
                    <TouchableOpacity style={styles.rightNav} onPress={ ()=>{rightAction()} }>
                      <View style={{alignItems: 'center'}}>
                       
                        <Image source={ rightImage } style={{width:18,height:18}}/>
                      </View>
                    </TouchableOpacity>
                            : null
                        )
                }
                {rightImage2 ?
                  <TouchableOpacity style={styles.rightNav2} onPress={ ()=>{rightAction2()} }>
                    <View style={{alignItems: 'center'}}>
                      <Image source={ rightImage2 } style={{width:24,height:24}}/>
                    </View>
                  </TouchableOpacity>
                  : null
                }
            </View>
            </View>
    )
  }
}

const styles = StyleSheet.create({
  barView: {
    height: naviHeight,
    // backgroundColor: themeconfig.maincolor,
    backgroundColor:"white",
    borderBottomColor: '#eee',
    borderBottomWidth: 1,
  },
  showView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: paddingTop,
    height: 44,
  },
  title: {
    color: '#4e4e4e',
    fontSize: 18.0,
  },
  leftNav: {
    position: 'absolute',
    top: 8,
    bottom: 8,
    left: 10,
    justifyContent: 'center',
    paddingHorizontal: 5
  },
  rightNav: {
    position: 'absolute',
    right: 8,
    top: 8,
    bottom: 8,
    justifyContent: 'center',
  },
  rightNav2: {
    position: 'absolute',
    right: 45,
    top: 8,
    bottom: 8,
    justifyContent: 'center',
  },
  barButton: {
    color: 'white',
    fontSize: 18
  },
})



export default NavigationBar