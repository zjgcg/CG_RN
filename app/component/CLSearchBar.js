import React, {
    Component
} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableWithoutFeedback,
    TouchableOpacity
} from 'react-native';

import {width, height, totalSize} from 'react-native-dimension';

const InputHeight = 28

class CLSearchBar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let placeholder = "区域/面积/租金/商铺";
        if (this.props.placeholder != undefined) {
            placeholder = this.props.placeholder;
        }
        return <TouchableWithoutFeedback onPress={() => {
            this.props.onSearchPress()
        }}>
            <View style={styles.searchBtn}>
                <Image style={{width: 20, height: 20}} source={require('../image/search.png')}/>
                <Text style={{fontSize: 12, color: "#cdcdcd", marginLeft: 5, flex: 1}}>{placeholder}</Text>
                {this.props.onCancel == undefined ? undefined : <TouchableOpacity onPress={this.props.onCancel}>
                    <Text style={{fontSize: 12, color: "#cdcdcd", marginRight: 5}}>取消</Text>
                </TouchableOpacity>}
            </View>
        </TouchableWithoutFeedback>
    }
}

const styles = StyleSheet.create({
    searchBtn: {
        paddingLeft: 10,
        height: InputHeight,
        flexDirection: "row",
        backgroundColor: "#eee",
        alignItems: "center",
        borderColor: '#eee',
        borderWidth: 1,
        borderRadius: 3,
    },
});

export default CLSearchBar;


    