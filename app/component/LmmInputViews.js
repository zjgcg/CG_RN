/**
 * Created by xmgong on 2018/6/29.
 * Lmm输入控件
 */

import {TextInput} from 'react-native';
import React, {
    Component
} from 'react';
import { StringUtil } from '../utils/common';

// 输入整数
export const InputInt = ({...props}) => {
    return <TextInput
        {...props}
        onChangeText={(text) => {
            const newText = text.replace(/[^\d]+/, '');
            props.onChangeText(newText);
        }
        }
    />
};

// 输入浮点数
export const InputFloat = ({...props}) => {
    return <TextInput
        {...props}
        maxLength={12}
        onChangeText={(text) => {
            // obj = obj.replace(/[^\d.]/g, "");
            // //必须保证第一位为数字而不是.
            // obj = obj.replace(/^\./g, "");
            // //保证只有出现一个.而没有多个.
            // obj = obj.replace(/\.{2,}/g, ".");
            // //保证.只出现一次，而不能出现两次以上
            // obj = obj.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
            // const newTxt = text.replace(/[^\d.]/g, "").replace(/^\./g, "").replace(/\.{2,}/g, ".").replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
            const newTxt = StringUtil.overToDecimal2(text);
            console.log(newTxt);
            props.onChangeText(newTxt);
        }
        }
    />
};
