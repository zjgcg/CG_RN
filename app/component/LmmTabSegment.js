import React from 'react';
import {
    StyleSheet,
} from 'react-native';
import SegmentedControlTab from 'react-native-segmented-control-tab'

const styles = StyleSheet.create({
    tabsContainerStyle: {
        flex: 1,
        backgroundColor: '#f9f9f9',
        borderWidth: 0,
        height: 50,
        borderRadius: 0,
    },
    tabStyle: {
        backgroundColor: '#f9f9f9',
        borderWidth: 0,
        paddingTop: 15,
        paddingBottom: 15,
    },
    tabTextStyle: {
        color: '#838383'
    },
    activeTabStyle: {
        backgroundColor: '#f9f9f9',
        borderBottomWidth: 4,
        marginBottom: -4,
        borderBottomColor: '#de2b30',
    },
    activeTabTextStyle: {
        color: '#e22222'
    },
    tabBadgeContainerStyle: {
        //custom styles
    },
    activeTabBadgeContainerStyle: {
        //custom styles
    },
    tabBadgeStyle: {
        //custom styles
    },
    activeTabBadgeStyle: {
        //custom styles
    }

})

class LmmTabSegment extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: 0,
        };
    }

    handleIndexChange = (index) => {
        this.setState({
            ...this.state,
            selectedIndex: index,
        });
        this.props.onTabPress(index);
    }

    render() {

        let selectedIndex = this.props.index || this.state.selectedIndex;

        return (<SegmentedControlTab
                tabStyle={styles.tabStyle}
                tabTextStyle={styles.tabTextStyle}
                activeTabStyle={styles.activeTabStyle}
                activeTabTextStyle={styles.activeTabTextStyle}
                selectedIndex={selectedIndex}
                values={['生意出售', '商铺租售', '厂房租售']}
                onTabPress={this.handleIndexChange}
            />
        );
    }
}

export default LmmTabSegment