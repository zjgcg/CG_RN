import React, {Component} from 'react';
import {
    Image,
    Platform,
} from 'react-native';

import FastImage from 'react-native-fast-image'

const LmmImageView = ({source, style, defaultImg = 'default.png'}) => {
    if (source.uri == undefined || source.uri == "") {
        let emptySource;
        if (defaultImg == 'default.png') {
            emptySource = require('../image/default.png')
        } else {
            emptySource = require('../image/default_userhead.png');
        }
        return <Image style={style}
                      source={emptySource}/>
    } else {
        if (Platform.OS == "ios") {
            return <FastImage
                style={style}
                source={{uri: source.uri, defaultImage: defaultImg}}
                resizeMode={FastImage.resizeMode.conver}/>
        } else {
            return <Image
                style={style}
                source={source}/>
        }
    }
};

export default LmmImageView;
