import React, {
    Component
  } from 'react';
  import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity
  } from 'react-native';

import { width, height, totalSize } from 'react-native-dimension';
  
const InputHeight = 28
  
class CLBorderButton extends Component {
    constructor(props) {
      super(props);
    }

    render() {
        return <TouchableOpacity onPress={()=>{
          if(this.props.onClick != undefined){
            this.props.onClick()
          }
        }}>
        <View style={[styles.buttonContainer,this.props.customStyle]}>
            <Text style={[{fontSize: 15, color:"orange"},this.props.customTextStyle]}>{this.props.title}</Text>
        </View>
      </TouchableOpacity>
    }
}

const styles = StyleSheet.create({
      buttonContainer: {
        borderWidth:1,
        borderColor:"orange",
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10,
        borderRadius: 3,
      },
});

export default CLBorderButton;