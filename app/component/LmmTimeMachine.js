import React from 'react';
import {
    View, Text, TextInput, TouchableOpacity, Image, Dimensions, ScrollView
} from 'react-native';

const LmmTimeMachine = ({items}) => {
    return <ScrollView horizontal={true}
                       showsHorizontalScrollIndicator={false}>
        {
            items.map((item, index) => {
                return <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{flexDirection: 'column', width: 80, justifyContent: 'center', alignItems: 'center'}}>
                        <Image style={{width: 30, height: 30, borderRadius: 15}} source={require('../image/shtx.png')}/>
                        <Text style={{fontSize: 13, color: 'gray', marginTop: 10}}>
                            {item.seller}
                        </Text>
                        <Image style={{width: 10, height: 10, marginTop: 3}}
                               source={require('../image/icon_empty_star.png')}/>
                        <Text style={{fontSize: 11, color: 'black', marginTop: 3}}>
                            {item.stepName}
                        </Text>
                        <Text style={{fontSize: 9, color: 'gray', marginTop: 3}}>
                            {item.datetime}
                        </Text>
                    </View>
                    {index != items.length - 1 ?
                        <Image style={{width: 30, height: 3, marginTop: 15}} source={require('../image/jt.png')}/>
                        : null
                    }
                </View>
            })
        }
    </ScrollView>
}

export default LmmTimeMachine;