import React from 'react';
import {connect} from 'react-redux';
import {
    View,
    FlatList,
    Text,
    RefreshControl,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Image,
    Platform,
    StyleSheet,
    Animated,
    Dimensions,
    ScrollView
} from 'react-native';

import themeconfig from '../config/theme';
import { toast } from '../utils/NativeUtil';

let {width, height} = Dimensions.get('window');

export class LmmSiftItemSolo extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showIndex: props.selectedIndex
        }
    }

    componentWillReceiveProps(props) {
        this.setState({showIndex: props.selectedCode});
    }

    renderSeparator = () => <View style={{height: 1, backgroundColor: '#eeeeee'}}/>

    renderItem = (item) => {
        let area = item.item;
        let index = item.index;

        return <TouchableOpacity style={{height: 40, justifyContent: 'center', padding: 10}} onPress={() => {
            this.props.select(item);
        }}>
            <Text style={{
                fontSize: 16,
                color: this.state.showIndex == index ? themeconfig.mainTextColor : 'black'
            }}>{area}</Text>
        </TouchableOpacity>
    }

    render() {
        return <FlatList style={{flex: 1, backgroundColor: 'white'}}
                         scrollEnabled={this.props.scrollEnabled}
                         ItemSeparatorComponent={this.renderSeparator}
                         data={this.props.data}
                         renderItem={this.renderItem}
        />
    }

}

export class LmmSiftItemDuo extends React.Component {

    constructor(props) {
        super(props);

        let dataList = JSON.parse(props.data);
        if (props.selectedCode.length == 0) {
            selectedCode = dataList.map(() => {
                return [];
            });
        } else {
            selectedCode = props.selectedCode.map((item, index) => {
                return item.map((item, index) => {
                    return item;
                });
            })
        }
        this.state = {
            selectedCode: selectedCode
        }
    }

    componentWillReceiveProps(props) {
        let dataList = JSON.parse(props.data);
        let selectedCode = props.selectedCode;
        if (selectedCode.length == 0) {
            this.setState({selectedCode: dataList.map(() => {
                return [];
            })});
        } else {
            this.setState({
                selectedCode: selectedCode.map((item, index) => {
                    return item.map((item, index) => {
                        return item;
                    });
                })
            });
        }
    }

    renderItem = (item, index, max) => {
        let isSelected = false;
        this.state.selectedCode[index].forEach(element => {
            if (item.ItemValue == element) {
                isSelected = true;
            }
        });

        return <TouchableOpacity
            style={{width: (width - 16) / 3, paddingHorizontal: 10, paddingVertical: 7.5}}
            onPress={() => {
                let selectedCodeArr = this.state.selectedCode;
                let selectedCode = selectedCodeArr[index];
                if (isSelected) {
                    selectedCode.splice(selectedCode.indexOf(item.ItemValue), 1);
                } else {
                    if (selectedCode.length < max) {
                        selectedCode.push(item.ItemValue);
                    } else {
                        toast('最多选择' + max + '项');
                    }
                }
                selectedCodeArr[index] = selectedCode;
                this.setState({selectedCode: selectedCodeArr});
            }}>
            <View style={{
                flex: 1,
                height: 29,
                borderWidth: 1,
                borderColor: isSelected ? '#e4393c' : 'lightgray',
                borderRadius: 5,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Text style={{fontSize: 13, color: isSelected ? '#e4393c' : '#666666'}}>{item.ItemText}</Text>
            </View>
        </TouchableOpacity>
    }

    render() {
        let dataList = JSON.parse(this.props.data);

        return <View style={{flex: 1, backgroundColor: 'white'}}>
            {/* <FlatList style={{flex: 1, paddingHorizontal: 8, paddingVertical: 12.5}}
             data={this.props.data}
             scrollEnabled={this.props.scrollEnabled}
             numColumns={3}
             renderItem={this.renderItem}
             /> */}
            <ScrollView style={{flex: 1, paddingHorizontal: 8}}
                        scrollEnabled={this.props.scrollEnabled}>
                {dataList.map((item1, i) => {
                    return (
                        <View>
                            <Text style={{
                                fontSize: 15,
                                color: '#333333',
                                marginVertical: 5,
                                marginHorizontal: 10
                            }}>{item1.type}</Text>
                            <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                                {item1.data.map((item2, j) => {
                                    return this.renderItem(item2, i, item1.max);
                                })}
                            </View>
                        </View>
                    )
                })}
            </ScrollView>
            {/*<TouchableOpacity style={{height: 15, marginTop: 5, alignItems: 'center', justifyContent: 'center'}}*/}
                              {/*onPress={() => {*/}
                                  {/*this.setState({selectedCode: []});*/}
                              {/*}}>*/}
                {/*<Text style={{color: '#e4393c', fontSize: 15}}>清空条件</Text>*/}
            {/*</TouchableOpacity>*/}
            <TouchableOpacity style={{
                height: 44,
                marginTop: 10,
                backgroundColor: '#e4393c',
                justifyContent: 'center',
                alignItems: 'center'
            }} onPress={() => {
                this.props.select(this.state.selectedCode);
            }}>
                <Text style={{fontSize: 17, color: 'white'}}>确定</Text>
            </TouchableOpacity>
        </View>
    }

}

export class LmmSiftItemSingle extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedCode: props.selectedCode.map((item, index) => {
                return item;
            })
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            selectedCode: props.selectedCode.map((item, index) => {
                return item;
            })
        });
    }

    renderItem = ({item}) => {
        let isSelected = false;
        this.state.selectedCode.forEach(element => {
            if (item.ItemValue == element) {
                isSelected = true;
            }
        });

        return <TouchableOpacity style={{width: (width - 16) / 3, paddingHorizontal: 10, paddingVertical: 7.5}}
                                 onPress={() => {
                                     let selectedCode = this.state.selectedCode;
                                     if (isSelected) {
                                         selectedCode.splice(selectedCode.indexOf(item.ItemValue), 1);
                                     } else {
                                         selectedCode.push(item.ItemValue);
                                     }

                                     this.setState({selectedCode: selectedCode});
                                 }}>
            <View style={{
                flex: 1,
                height: 29,
                borderWidth: 1,
                borderColor: isSelected ? '#e4393c' : 'lightgray',
                borderRadius: 5,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Text style={{fontSize: 13, color: isSelected ? '#e4393c' : '#666666'}}>{item.ItemText}</Text>
            </View>
        </TouchableOpacity>
    }

    render() {

        return <View style={{flex: 1, backgroundColor: 'white'}}>
            <FlatList style={{flex: 1, paddingHorizontal: 8, paddingVertical: 12.5}}
                      data={this.props.data}
                      scrollEnabled={this.props.scrollEnabled}
                      numColumns={3}
                      renderItem={this.renderItem}
            />
            {/*<TouchableOpacity style={{height: 15, marginTop: 5, alignItems: 'center', justifyContent: 'center'}}*/}
                              {/*onPress={() => {*/}
                                  {/*this.setState({selectedCode: []});*/}
                              {/*}}>*/}
                {/*<Text style={{color: '#e4393c', fontSize: 15}}>清空条件</Text>*/}
            {/*</TouchableOpacity>*/}
            <TouchableOpacity style={{
                height: 44,
                marginTop: 10,
                backgroundColor: '#e4393c',
                justifyContent: 'center',
                alignItems: 'center'
            }} onPress={() => {
                this.props.select(this.state.selectedCode);
            }}>
                <Text style={{fontSize: 17, color: 'white'}}>确定</Text>
            </TouchableOpacity>
        </View>
    }

}

export default class LmmSiftView extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showIndex: -1,
            componentHeight: new Animated.Value(0)
        }
    }

    componentWillReceiveProps() {
        this.setState({showIndex: -1});
    }

    tap(index) {
        if (this.state.showIndex != -1 && this.state.showIndex == index) {
            this.endAnimation();
        } else {
            this.setState({showIndex: index});
            if (this.state.showIndex == -1) {
                this.startAnimation(index);
            } else {
                this.state.componentHeight.setValue(this.props.items[index].height);
            }
        }
    }

    startAnimation = (index) => {
        this.state.componentHeight.setValue(0);
        Animated.timing(this.state.componentHeight, {toValue: this.props.items[index].height, duration: 300}).start();
    }

    endAnimation = () => {
        let onCancel = this.props.items[this.state.showIndex].onCancel;
        if (onCancel != undefined) {
            onCancel();
        }
        this.setState({showIndex: -1});
    }

    render = () => {
        return <View style={{flex: 1}}>
            <View style={{height: 40, flexDirection: 'row', backgroundColor: 'white'}}>
                {this.props.items.map((item, index) => {
                    return <TouchableWithoutFeedback onPress={() => this.tap(index)}>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{
                                fontSize: 13,
                                color: this.state.showIndex == index ? themeconfig.mainTextColor : '#333333'
                            }} numberOfLines={1}>{item.title}</Text>
                            <Image style={{width: 20, height: 20}} source={require('../image/down.png')}/>
                        </View>
                    </TouchableWithoutFeedback>
                })}
            </View>
            <View style={{height: 1, backgroundColor: '#eeeeee'}}/>
            {this.props.children}
            {this.state.showIndex == -1 ? undefined : <TouchableWithoutFeedback onPress={this.endAnimation}>
                <View
                    style={{position: "absolute", left: 0, right: 0, top: 41, bottom: 0, backgroundColor: '#00000055'}}>
                    <Animated.View
                        style={{overflow: 'hidden', height: this.state.componentHeight, backgroundColor: '#ffffff00'}}>
                        {this.props.items[this.state.showIndex].component}
                    </Animated.View>
                </View>
            </TouchableWithoutFeedback>}
        </View>
    }
}


const styles = StyleSheet.create({
    searchView: {
        height: 40,
        backgroundColor: "white"
    },
    tagViewContainer: {
        height: 40,
        backgroundColor: '#eee',
        flexDirection: "row",
    },
    tagView: {
        borderWidth: 1,
        borderColor: "#aaa",
        height: 24,
        padding: 5,
        margin: 8,
        flexDirection: "row",
        alignItems: "center"
    }
});

