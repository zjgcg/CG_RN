/**
 * Created by xmgong on 2018/4/7.
 */
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

class lmmtext extends React.Component {

    constructor(props) {
        super(props);
    }


    render = () => {

        let valueView = null;
        let hintView = null;
        let value = this.props.value;
        let hinttext = this.props.hinttext;

        if (value.length > 0) {
            valueView = (<Text style={{color: 'black', fontSize: 14}}>{value}</Text>);
            hintView = null;
        } else {
            valueView = null;
            hintView = (<Text style={{color: '#a3a3a3' , fontSize:14 }}>{hinttext}</Text>)
        }

        return (<View style={{flexDirection: 'row', alignItems: 'center'}}>
            {valueView}
            {hintView}
        </View>)
    }
}

export default lmmtext;