import React, {
    Component
  } from 'react';
  import {
    View,
    Platform,
    KeyboardAvoidingView
  } from 'react-native';

class LmmKeyboardAvoidingView extends Component {
    constructor(props) {
      super(props);
    }

    render() {
        if (Platform.OS == "ios"){
            return <KeyboardAvoidingView style={{flex:1}}
                                         behavior={this.props.behavior || 'padding'}
                                         keyboardVerticalOffset={ this.props.keyboardVerticalOffset }>
                {this.props.children}
            </KeyboardAvoidingView>
        }else{
            return <View style={{flex:1}}>
                {this.props.children}
            </View>
        }
    }
}

export default LmmKeyboardAvoidingView;