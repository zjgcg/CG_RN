import React, {
    Component
  } from 'react';
  import {
    View,FlatList,InteractionManager
  } from 'react-native';

import CLTableFootView from './CLTableFootView'

class LmmFlatListView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            footText: "",
            refreshing: false,
            isLoading:true,
            list:[],
        }
        this.page = 1;
        this.hasMore = true;
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.loadData();
        });
    }

    _renderFooter = () => {
        return (
          <CLTableFootView isLoading={this.state.isLoading} footText = {this.state.footText} />
        )
    }

    _onEndReached = () => {
        if (this.hasMore && !this.state.isLoading) {
          this.setState({isLoading: true});
          this.loadData();
          this.hasMore = false;
        }
    }
    
    _onRefresh = () => {
        this.page = 1;
        this.setState({refreshing:true,list:[],footText:"正在加载"});
        this.loadData();
    }

    _keyExtractor = (item) => {
        return item[this.props.id]
    }

    _renderSeparator = () => {
        return <View style={{backgroundColor:"#eee",height:1,flex:1}}></View>
    }

    loadData = () => {

        if (!this.hasMore){
            return;
        }

        if (this.props.loadData != undefined){
            this.props.loadData(this.page).then((value) => {
                if (value){
                    let type = 1;
                    let data = value;

                    if (this.page == 1){
                        if (data.length == 0){
                            type = 0;
                        }
                    }else{
                        if (data.length < 10){
                            type = 2;
                        }else{
                            type = 1;
                        }
                    }

                    if (this.page != 1){
                        data = this.state.list.concat(value);
                    }else{
                        data = value;
                    }

                    this.page += 1;

                    if(type == 1){
                        this.hasMore = true;
                        this.setState({ 
                            list:data,    
                            refreshing:false,
                            isLoading:false, 
                            footText:"上拉加载更多"});
                    }else if(type == 2){
                        this.hasMore = false;
                        this.setState({
                            list:data,
                            refreshing:false,
                            isLoading:false, 
                            footText:"加载完成"});
                    }else if(type == 0){
                        this.hasMore = false;
                        this.setState({
                            list:data,
                            refreshing:false,
                            isLoading:false, 
                            footText:"暂无数据"
                        });
                    }
                    
                }
            })
        }
    }

    render() {
        return <View style={{flex:1,backgroundColor:'white'}}>
        <FlatList
            style = {{flex:1}}
            data={this.state.list}
            renderItem={this.props.renderItem}
            keyExtractor={this._keyExtractor}
            ListFooterComponent={this._renderFooter}
            onEndReached={this._onEndReached}
            ItemSeparatorComponent={this._renderSeparator}
            onEndReachedThreshold={1}
            onRefresh={this._onRefresh}
            refreshing={this.state.refreshing}></FlatList>
        </View>
    }
}

export default LmmFlatListView;