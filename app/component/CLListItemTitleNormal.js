import React, {
    Component
  } from 'react';
  import {
    Modal,
    TextInput,
    View,
    Image,
    Text,
    StyleSheet,
    TouchableHighlight
  } from 'react-native';

import { width, height, totalSize } from 'react-native-dimension';
  
  const styles = StyleSheet.create({
        listItemContainer:{
            alignItems: "center",
            flexDirection: "row",
            justifyContent: "space-between",
            height:49,
        },
        listItemLeft:{
            alignItems: "center",
            flexDirection: "row",
            marginLeft:15,
        },
  });

  const CLListItemTitle = ({title,subTitle,icon,noNeedRightIcon}) => (
        <View style={styles.listItemContainer}>
                <View style={styles.listItemLeft}>
                    {
                        icon == undefined ? null:
                        <Image style={{width:24,height:24}} source={icon} />
                    }
                    <Text style={{marginLeft:10, fontSize: 15, color: '#333333'}}>{title}</Text>
                </View>
                <View style={{marginRight:15,     
                    alignItems: "center",
                    flexDirection: "row",}} >
                    {subTitle!=undefined?
                        <Text style={{color: '#999999', fontSize: 14}}>{subTitle}</Text>
                        :
                        null
                    }
                    {/* {
                        noNeedRightIcon != undefined && noNeedRightIcon ? null :
                        <Image source={require('../image/more.png')} style={{marginLeft:10,width:8,height:10}}/>
                    } */}
                    
                </View>
        </View>
  )

  export default CLListItemTitle;