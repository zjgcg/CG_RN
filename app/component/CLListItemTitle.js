import React, {
    Component
  } from 'react';
  import {
    Modal,
    TextInput,
    View,
    Image,
    Text,
    StyleSheet,
    TouchableHighlight
  } from 'react-native';

import { width, height, totalSize } from 'react-native-dimension';
  
  const styles = StyleSheet.create({
        listItemContainer:{
            alignItems: "center",
            flexDirection: "row",
            height:30,
        },
        listItemLeft:{
            alignItems: "center",
            flexDirection: "row",
        },
  });

  const CLListItemTitle = ({title,subTitle,icon,customFontStyle}) => (
        <View style={styles.listItemContainer}>
        <View style={{width:2,backgroundColor:"red",height:30}}>
        </View>
                <View style={styles.listItemLeft}>
                   
                    {
                        icon == undefined ? null:
                        <Image style={{width:24,height:24}} source={icon} />
                    }
                    {
                        customFontStyle == undefined ? 
                        <Text style={{marginLeft:10}}>{title}</Text>
                        :
                        <Text style={[{marginLeft:10},customFontStyle]}>{title}</Text>

                    }
                    
                </View>
        </View>
  )

  export default CLListItemTitle;