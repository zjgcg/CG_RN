import React, {
    Component
  } from 'react';
  import {
    Modal,
    TextInput,
    View,
    Image,
    Text,
    StyleSheet,
    TouchableHighlight,
    TouchableWithoutFeedback
  } from 'react-native';

import { width, height, totalSize } from 'react-native-dimension';
  
  const styles = StyleSheet.create({
        listItemContainer:{
            flex:1,
            justifyContent: "space-between",
            alignItems: "center",
            flexDirection: "row",
            height:36,
        },
        listItemLeft:{
            flex:1,
            alignItems: "center",
            flexDirection: "row",
            marginLeft:10,
        },
        listItemRight:{
            marginRight:10,
            alignItems: "center",
            flexDirection: "row",
        },
  });

  const CLListItem = ({title,subTitle,icon,rightIcon,onPress}) => (
      <TouchableWithoutFeedback onPress={() => {onPress()}}>
        <View style={styles.listItemContainer}>
        <View style={{width:2,backgroundColor:"red",height:36}}>
        </View>
                <View style={styles.listItemLeft}>
                   
                    {
                        icon == undefined ? null:
                        <Image style={{width:24,height:24}} source={icon} />
                    }
                    <Text style={{marginLeft:10}}>{title}</Text>
                </View>
                <View style={styles.listItemRight}>
                {subTitle!=undefined?
                    <Text style={{color:'gray'}}>{subTitle}</Text>
                    :
                    null
                }
                {rightIcon == undefined?
                    <Image style={{width:(5),height:10}} source={require('../image/more.png')} />
                    :
                    <Image style={{width:24,height:24}} source={rightIcon} />
                }
                </View>
        </View>
      </TouchableWithoutFeedback>
  )

  export default CLListItem;