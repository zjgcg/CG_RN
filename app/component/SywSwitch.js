/**
 * Created by xmgong on 2018/4/17.
 */

import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import theme from '../config/theme';

const styles = StyleSheet.create({
    checkedStyle: {
        width: this.itemWidth,
        height: this.itemHeight,
        backgroundColor: theme.redColor,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
    },
    checkedTxtStyle: {
        color: 'white'
    },

    uncheckedStyle: {
        width: this.itemWidth,
        height: this.itemHeight,
        backgroundColor: '#eee',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: 'center',
    },
    uncheckedTxtStyle: {
        color: '#333333'
    },
    leftStyle: {
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
    },
    rightStyle: {
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        borderTopRightRadius: 3,
        borderBottomRightRadius: 3
    }
});

export default class sywSwitch extends React.Component {

    constructor(props) {
        super(props);
        this.itemWidth = props.itemwidth || 100;
        this.itemHeight = props.itemheight || 40;
        this.options = props.options;
        this.checkedStyle = props.checkedstyle;
        this.checkedTxtStyle = props.checkedtxtstyle;
        this.uncheckedStyle = props.uncheckedstyle;
        this.uncheckedTxtStyle = props.uncheckedtxtstyle;
    }

    render = () => {
        let index = this.props.index;
        let views = this.options.map((item, i) => {
            if (i == index) {
                return (<TouchableOpacity
                    onPress={() => this.props.onChangeIndex(i)}
                    style={[this.checkedStyle, styles.checkedStyle,
                        i == 0 ? styles.leftStyle : null, i == this.options.length - 1 ? styles.rightStyle : null]}>
                    <Text style={[this.checkedTxtStyle, styles.checkedTxtStyle]}>{item}</Text>
                </TouchableOpacity>);

            } else {
                return (<TouchableOpacity
                    onPress={() => this.props.onChangeIndex(i)}
                    style={[i == 0 ? styles.leftStyle : null, i == this.options.length - 1 ? styles.rightStyle : null,
                        styles.uncheckedStyle,
                        this.uncheckedStyle]}>
                    <Text style={[this.uncheckedTxtStyle, styles.uncheckedTxtStyle]}>{item}</Text>
                </TouchableOpacity>)
            }
        });

        return (<View style={{flexDirection: 'row', height: this.itemHeight, borderRadius: 3}}>
            {views}
        </View>)
    }
}