/**
 * Created by xmgong on 2018/4/17.
 * 生意网选择图片
 */


import React from 'react';
import {View, TouchableOpacity, Dimensions, Image, Text} from 'react-native';
import LmmImageView from './LmmImageView';
const {width, height} = Dimensions.get('window');

export  default class sywchoosepic extends React.Component {

    constructor(props) {
        super(props);
        this.Number = props.num || 3;
        this.ItemHeight = props.itemheight || 60;

    }

    render = () => {
        let self = this;
        let images = this.props.images;
        let imgviews = images.map((item, i) => {
            return self.renderItem(item, i);
        });

        return (<View
            style={{flexWrap: 'wrap', flexDirection: 'row', justifyContent: 'flex-start',}}>
            {imgviews}
        </View>)
    };


    itemClick = (item) => {
        this.props.onItemClick(item);
    };


    renderItem = (item, i) => {
        let itemWidth = (width - 40 - (this.Number - 1) * 10 ) / this.Number;
        let itemview;
        let txtview;
        if (item.title && item.title.length > 0) {
            txtview = (<Text style={{fontSize: 12}}>{item.title}</Text>);
        }
        if (item.url == '') {
            itemview = (
                <View style={{flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
                    <Image source={require('../image/add_51.png')} style={{width: 30, height: 30, marginBottom: 3}}/>
                    {txtview}
                </View>);
        } else {
            itemview = (<LmmImageView source={{uri: item.url}} style={{
                width: itemWidth,
                height: this.ItemHeight,
            }}/>);
        }

        if ((i + 1) % this.Number > 0) {
            return (
                <TouchableOpacity
                    onPress={() => this.itemClick(item)}
                    style={{
                        width: itemWidth,
                        height: this.ItemHeight,
                        backgroundColor: '#eee',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                        borderColor: '#eee',
                        marginRight: 10,
                        marginBottom: 10
                    }}>
                    {itemview}
                </TouchableOpacity>);
        } else {
            return (
                <TouchableOpacity
                    onPress={() => this.itemClick(item)}
                    style={{
                        width: itemWidth,
                        height: this.ItemHeight,
                        backgroundColor: '#eee',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                        borderColor: '#eee',
                        marginBottom: 10
                    }}>
                    {itemview}
                </TouchableOpacity>);
        }
    };
}

