import React, {
    Component
  } from 'react';
import {
    View,
    Text,
    StyleSheet,
  } from 'react-native';


export default LmmCommonTitle = ({style,title,children}) => (
            <View style={[styles.top,style]}>
                <Text numberOfLines={1} style={{color:"black",fontSize:16}}>{title}</Text>
                {children}
            </View>
)

const styles = StyleSheet.create({
    top:{
        padding:5,
        backgroundColor:"#eee",
        flexDirection:"row",
        justifyContent: "space-between",
        alignItems:"center",
        height:26,
    },
});