import React, {
    Component
} from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    Platform,
    TouchableOpacity
} from 'react-native';

import themeconfig from '../config/theme'
import {naviHeight, paddingTop} from '../utils/ScreenUtil';


class CLNavigatorHeaderWithCustom extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        // leftTitle和leftImage 优先判断leftTitle (即 文本按钮和图片按钮优先显示文本按钮)
        const {
            title,
            leftTitle,
            leftAction,
            titleAction,
            rightTitle,
            titleStyle,
            rightAction
        } = this.props;
        return (
            <View style={[styles.barView, this.props.customStyle]}>
                <View style={ styles.showView }>
                    <TouchableOpacity style={styles.leftNav} onPress={ () => {
                        leftAction()
                    } }>
                        {leftTitle()}
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex: 1}} onPress={ () => {
                        titleAction()
                    } }>
                        {title()}
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.rightNav} onPress={ () => {
                        rightAction()
                    } }>
                        {rightTitle()}
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    barView: {
        height: naviHeight,
        // backgroundColor: themeconfig.maincolor,
        backgroundColor: 'white',
        borderBottomColor: "#eee",
        borderBottomWidth: 1,
        flexDirection: 'row',
    },
    showView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: paddingTop,
        height: 44,
    },
    leftNav: {
        justifyContent: 'center',
    },
    rightNav: {
        justifyContent: 'center',
    },
    barButton: {
        color: '#4e4e4e',
        fontSize: 18
    },
})


export default CLNavigatorHeaderWithCustom