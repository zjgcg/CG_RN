import React from 'react';
import {
    View, Text, TextInput, TouchableOpacity, Image, Dimensions
} from 'react-native';

const LmmTextInput = ({title,value,onChangeText}) => {
    return (<View>
        <View style={{
            height: 10,
            flexDirection:'row',
            alignItems:'center',
            marginBottom:-5,
            }}>

            <View style={{height:1,backgroundColor:'#d7d7d7',width:10}}>
            </View>

            <Text style={{color:'gray',fontSize:18}}>
            {title}
            </Text>

            <View style={{height:1,backgroundColor:'#d7d7d7',flex:1}}>
            </View>

        </View>
        
        <TextInput
                style={{
                    borderRadius: 3,
                    height: 50,
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingLeft: 10, paddingRight: 10,
                    borderColor: '#d7d7d7',
                    borderWidth: 1,
                    borderTopWidth:0
                    }}
                    value={value}
                    underlineColorAndroid='transparent'
                    onChangeText={(txt) => {
                            onChangeText(txt)
                    }}
                    keyboardType='numeric'
                    multiline={false}
                    />
        </View>            
    )
}

export default LmmTextInput;