import React from 'react';
import {connect} from 'react-redux';
import {
    View,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    ProgressViewIOS
} from 'react-native';

import Video from 'react-native-video'

const {width,height}  = Dimensions.get('window'); 

var styles = StyleSheet.create({
    backgroundVideo: {
      width:width,
      height:width/2,
    },
});

class LmmVideo extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            currentTime:"00:00",
            allTime:"00:00",
            paused:true,
            progress:0,
        }
    }

    onLoad = (value) => {
        console.log(value)
    }

    setTime = (time) => {
        let progress = time.currentTime / time.seekableDuration;
        this.setState({
            currentTime:this.convertTime(time.currentTime),
            allTime:this.convertTime(time.seekableDuration),
            progress:progress,
        })
    }

    convertTime = (time) => {
        if (time < 60){
            if (time < 10){
                return "00:0"  + parseInt(time)
            }else{
                return "00:"  + parseInt(time)
            }
        }
    }

    render() {
        return <View>
            <Video source={{
                uri: "http://p20oh26xk.bkt.clouddn.com/4b4c985d33a40deb8812e979dce7f8e1.mp4"}}   // Can be a URL or a local file. 
                ref={(ref) => {
                    this.player = ref
                }}                                      // Store reference 
                rate={1.0}                              // 0 is paused, 1 is normal. 
                volume={1.0}                            // 0 is muted, 1 is normal. 
                muted={false}                           // Mutes the audio entirely. 
                paused={this.state.paused}                          // Pauses playback entirely. 
                resizeMode="cover"                      // Fill the whole screen at aspect ratio.* 
                repeat={false}                           // Repeat forever. 
                playInBackground={false}                // Audio continues to play when app entering background. 
                playWhenInactive={false}                // [iOS] Video continues to play when control or notification center are shown. 
                ignoreSilentSwitch={"ignore"}           // [iOS] ignore | obey - When 'ignore', audio will still play with the iOS hard silent switch set to silent. When 'obey', audio will toggle with the switch. When not specified, will inherit audio settings as usual. 
                progressUpdateInterval={250.0}     
                style={styles.backgroundVideo}  
                onLoad={this.setDuration}               // Callback when video loads 
                onProgress={this.setTime}               // [iOS] Interval to fire onProgress (default to ~250ms) 
            />

            <View style={{
                marginTop:-40,
                height:40,
                padding: 10,
                width:width,
                backgroundColor:"rgba(0, 0, 0, 0.5)",
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center"
            }}>

                {this.state.paused? <TouchableOpacity
                    onPress={() => {this.setState({paused:false})}}
                    >
                        <Image
                        style={{width:20,height:20}}
                        source={require('../image/play.png')}
                        />
                    </TouchableOpacity>
                :
                <TouchableOpacity
                    onPress={() => {this.setState({paused:true})}}
                    >
                    <Image
                    style={{width:20,height:20}}
                    source={require('../image/pause.png')}
                    />
                </TouchableOpacity>
                }

                <Text style={{color:"white"}}>
                    {this.state.currentTime}
                </Text>

                <ProgressViewIOS 
                    style={{width:width-200}}
                    progress={this.state.progress}
                />

                <Text style={{color:"white"}}>
                    {this.state.allTime}
                </Text>

                <TouchableOpacity
                    onPress={() => {this.player.presentFullscreenPlayer()}}
                    >
                    <Image
                    style={{width:20,height:20}}
                    source={require('../image/qp.png')}
                    />
                </TouchableOpacity>
            </View>

         </View>
    }
}

export default LmmVideo