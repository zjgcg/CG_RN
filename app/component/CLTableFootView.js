import React, {
    Component
  } from 'react';
  import {
    View,
    Text,
    Image,
    ActivityIndicator,
    StyleSheet,
  } from 'react-native';

const CLTableFootView = ({footText,isLoading,isEmpty}) => {

  //   let padding = 10;
  //   if (footText == "加载完成"){
  //       padding = 0;
  //   }

  //   return <View
  //   style={{
  //       padding: padding,
  //       backgroundColor:"white",
  //       borderTopColor: "#eee",
  //       borderTopWidth: 1,
  //       flex: 1,
  //       flexDirection: 'row',
  //       justifyContent: 'center',
  //       alignItems: "center",
  //   }}>
    
  //   {isEmpty?
  //       <View style={{
  //         flex: 1,
  //         flexDirection: 'column',
  //         justifyContent: 'center',
  //         alignItems: "center",
  //       }}>
  //          <Image style={{width:80,height:80,margin:10}} source={require('../image/empty.png')} />
  //          <Text style={{
  //           color: "gray"
  //         }}>暂无数据</Text>
  //       </View>
  //       :
  //       <View style={{flexDirection: 'row',alignItems: "center",}}>
  //       {
  //         isLoading?
  //         <ActivityIndicator hidesWhenStopped={true} animating={isLoading} />
  //         :null
  //       }
  //       {
  //         footText == "加载完成"?
  //         null:
  //         <Text style={{
  //           color: "gray"
  //         }}>{isLoading?"正在加载":footText}</Text>
  //       }
  //       </View>
  //   }
  // </View>

  let lineStyle = {};
    if (!isEmpty) {
       lineStyle = {
          borderTopColor: "#eee",
          borderTopWidth: 1,
       }
    }

    return <View
    style={[{
        padding: 10,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: "center",
    },lineStyle]}>
    {
      isLoading?
      <ActivityIndicator hidesWhenStopped={true} animating={isLoading} />
      :null
    }
    {
      isEmpty?
      null:
      <Text style={{
        color: "gray"
      }}>{isLoading?"正在加载":footText}</Text>
    }
    
  </View>
}

export default CLTableFootView;