import React, {
    Component
} from 'react';
import {
    Modal,
    TextInput,
    View,
    Image,
    Text,
    StyleSheet,
    TouchableHighlight
} from 'react-native';

import {width, height, totalSize} from 'react-native-dimension';

const styles = StyleSheet.create({
    listItemContainer: {
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
        height: 48,
    },
    listItemLeft: {
        alignItems: "center",
        flexDirection: "row",
        marginLeft: 10,
    },
});

const LmmListItemWithTitle = ({title, subTitle, noNeedRightIcon}) => (
    <View style={styles.listItemContainer}>
        <View style={styles.listItemLeft}>
            <Text style={{color: 'gray', fontSize: 13}}>{title}</Text>
        </View>
        <View style={{
            marginRight: 10,
            alignItems: "center",
            flexDirection: "row",
        }}>
            {subTitle != undefined ?
                <Text>{subTitle}</Text>
                :
                null
            }
            {
                noNeedRightIcon != undefined && noNeedRightIcon ? null :
                    <Image source={require('../image/more.png')} style={{marginLeft: 10, width: 10, height: 15}}/>
            }
        </View>
    </View>
)

export default LmmListItemWithTitle;