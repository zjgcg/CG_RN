import React, {
    Component
  } from 'react';
  import {
    View,
    Text,
    StyleSheet,
  } from 'react-native';

import { width, height, totalSize } from 'react-native-dimension';
  
const InputHeight = 28
  
class CLTagView extends Component {
    constructor(props) {
      super(props);
    }

    render() {
        if(this.props.title == undefined){
            return <View></View>
        }else if (this.props.title == ""){
            return <View></View>
        }else {
            return <View style={[styles.tagContainer,this.props.customStyle]}>
                <Text style={[{fontSize: 10.5, color:"#666666"},this.props.textStyle]}>{this.props.title}</Text>
            </View>
        }
    }
}

const styles = StyleSheet.create({
    tagContainer: {
        backgroundColor: '#eee',
        borderWidth:1,
        borderColor:"#efefef",
        height:15,
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 5,
        borderRadius: 3,
      },
});

export default CLTagView;