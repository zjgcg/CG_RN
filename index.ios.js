/**
 * Created by chenliang on 2017/1/16.
 */
'use strict';

import React from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,	
	View
} from 'react-native';

import ZhendeIOSApp from './app/root'

export default class Test extends React.Component {
	render() {
		return (
			<View style={{height: 50,
        		  		  marginBottom: 10,
        		  		  justifyContent: 'space-around',
        		  		  flexDirection: 'row',
        		  		  alignItems: 'center',
						  backgroundColor: 'yellow'}}>
				<View style={{backgroundColor: 'green', width: 38, height: 50}}>
					<View style={{backgroundColor: 'red', width: 38, height: 49}}/>
					<Text style={{color: 'white', textAlign: 'center'}}>消息</Text>
				</View>
				<View style={{backgroundColor: 'green', width: 38, height: 50}}>
					<View style={{backgroundColor: 'red', width: 38, height: 49}}/>
					<Text>消息</Text>
				</View>
				<View style={{backgroundColor: 'green', width: 38, height: 50}}>
					<View style={{backgroundColor: 'red', width: 38, height: 49}}/>
					<Text>消息</Text>
				</View>
			</View>
		)
	}
}

AppRegistry.registerComponent('ZhendeIOSApp', () => ZhendeIOSApp);

